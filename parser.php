<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Parser 
{
    private static $instance = NULL;
    
    private $action;
    private $array;
    private $url;
    private $order;
    private $order_by;
    private $search;
    private $page;

    
    /**
     * Constructor
     */
    function __construct() {
        $this->action = null;
        $this->array = null;
        $this->url = null;
        $this->order = null;
        $this->order_by = null;
        $this->search = null;
        $this->page = 0;
    }
    
    /**
     * Destructor
     */
    function __destruct() {
        ;
    }
    
    /**
     * Return instance of class
     * @return instance of class Parser
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) {
            self::$instance = new $class;
        }
        return self::$instance;
    }
    
    /**
     * Inicialization of class Parser
     */
    public function init() 
    {   
        if (isset($_GET['url']))
        {
            $url = $_GET['url'];
            $url = explode('/', $url);

            $this->url = '';
            $this->action = $url[0];
            $this->order = null;
            $this->order_by = null;
            $this->search = null;
            $this->page = 1;

            $array = array_slice($url, 1, count($url));
            $array_new = null;
            

            if ($this->action == ACTION_LIST)
            {
                $db = Database::get_instance();
                $db->connect();

                $category = DB_Category::get_instance();
                
                foreach ($array as $i => $row) 
                {
                    $data = $category->get_category($db, $row);
                    $data = mysqli_fetch_array($data);        
                    if ($data)
                    {
                        $array_new[] = $data;
                        $this->url .= '/' . $data['url'];
                    }
                }

                $db->disconnect();

                $this->array = $array_new;
                
                if(isset($_GET[ACTUAL_PAGE]))
                {
                    if (is_numeric($_GET[ACTUAL_PAGE]))
                    {
                        $this->page = $_GET[ACTUAL_PAGE];
                    }
                }
                
                if(isset($_GET[ORDER]))
                {
                    $this->order = $_GET[ORDER];
                }
                if(isset($_GET[ORDER_BY]))
                {
                    $this->order_by = $_GET[ORDER_BY];
                }
            }
            
            if ($this->action == ACTION_BILL_EDIT)
            {
                 foreach ($array as $i => $row) 
                {
                    $array_new[] = $row;
                    $this->url .= '/' . $row;                    
                }

                $this->array = $array_new;
            }
            
            if ($this->action == ACTION_BILL_DETAIL)
            {
                foreach ($array as $i => $row) 
                {
                    $array_new[] = $row;
                    $this->url .= '/' . $row;                    
                }

                $this->array = $array_new;
            }
            
            if ($this->action == ACTION_SEARCH)
            {
                $db = Database::get_instance();
                $db->connect();

                $category = DB_Category::get_instance();
//                $this->search = end($array);
                $this->search = $_GET['q'];
//                $array = array_slice($array, 0, count($array)-1);
                
                foreach ($array as $i => $row) 
                {
                    $data = $category->get_category($db, $row);
                    $data = mysqli_fetch_array($data);        
                    if ($data)
                    {
                        $array_new[] = $data;
                        $this->url .= '/' . $data['url'];
                    }
                }

                $db->disconnect();

                $this->array = $array_new;
                
                if(isset($_GET[ORDER]))
                {
                    $this->order = $_GET[ORDER];
                }
                if(isset($_GET[ORDER_BY]))
                {
                    $this->order_by = $_GET[ORDER_BY];
                }
                if(isset($_GET[ACTUAL_PAGE]))
                {
                    if (is_numeric($_GET[ACTUAL_PAGE]))
                    {
                        $this->page = $_GET[ACTUAL_PAGE];
                    }
                }
                
                $this->array = $array_new;
            }
            
            if ($this->action == ACTION_COMPANY)
            {                                
                if(isset($_GET[ACTUAL_PAGE]))
                {
                    if (is_numeric($_GET[ACTUAL_PAGE]))
                    {
                        $this->page = $_GET[ACTUAL_PAGE];
                    }
                }
            }
        }
        else {
            $this->action = ACTION_FRONT;
            $this->url = '';
            $this->array = NULL;
        }
    }
    
    /**
     * Get action 
     * @return string
     */
    public function get_action()
    {
        return $this->action;
    }
    
    /**
     * Get array of string
     * @return array[string]
     */
    public function get_array()
    {
        return $this->array;
    }
    
    /**
     * Get url adress
     * @return string
     */
    public function get_url()
    {
        return $this->url;
    }
    
    /**
     * Get columb to oreder
     * @return string
     */
    public function get_order()
    {
        return $this->order;
    }
    
    /**
     * Get type of order
     * @return string
     */
    public function get_order_by()
    {
        return $this->order_by;
    }
    
    /**
     * Get string for search
     * @return string
     */
    public function get_search()
    {
        return $this->search;
    }
    
    /**
     * Get number of current page
     * @return uint
     */
    public function get_page()
    {
        return $this->page;
    }
    
}
?>
