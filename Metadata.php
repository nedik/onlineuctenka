<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'Settings/conf.php';

/**
 * Class to generate metadatas
 */
class Metadata
{
    private static $instance = NULL;
    
    /** title of page */
    private $title;
    /** description of page */
    private $description;
    /** text of page */
    private $text;
    /** keywords of page */
    private $keywords;

    /**
     * Constructor
     */
    public function __construct() 
    {
        $this->title = conf::$server_name;
        $this->description = conf::$server_description;
        $this->text = '';
        $this->keywords = '';
    }
    
    /**
     * Destructor
     */
    function __destruct() {
        ;
    }
    
    /**
     * Return instance of class
     * @return instance of class Metadata
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) {
            self::$instance = new $class;
        }
        return self::$instance;
    }
    
    /**
     * Generation of metadatas
     * @param string $page  identification of page
     * @return array of metatadas
     */
    public function generate_metatag($page = NULL)
    {
        if ($page == PAGE_FRONT)
        {
            $this->title = 'Úvodní stránka ' . conf::$server_name;
            $this->description = 'Úvodní stránka webu';
            $this->text = '';
            $this->keywords = 'účtenka, účet, uložit';
        }
        
        if ($page == PAGE_LOGIN)
        {
            $this->title = 'Přihlášení do ' . conf::$server_name;
            $this->description = 'Přihlášení na web pos svým emailem';
            $this->text = '';
            $this->keywords = 'přihlášení';
        }
        
        if ($page == PAGE_REG)
        {
            $this->title = 'Registrace do ' . conf::$server_name;
            $this->description = 'Registruj se jako nový uživatel webu';
            $this->text = '';
            $this->keywords = 'registrace';
        }
        
        if ($page == PAGE_MAIN)
        {
            $this->title = 'Vítejte do ' . conf::$server_name;
            $this->description = 'Systém pro uložení Vašich účtenek';
            $this->text = '';
            $this->keywords = 'účtenka, účet';
        }
        
        if ($page == PAGE_BILL_CREATE)
        {
            $this->title = 'Vytvoření nové účtenky v ' . conf::$server_name;
            $this->description = 'Vytvoření nové účtenky na webu';
            $this->text = '';
            $this->keywords = 'účtenka, účet, vytvoření';
        }
        
        if ($page == PAGE_BILL_DETAIL)
        {
            $this->title = 'Detail účtenky na ' . conf::$server_name;
            $this->description = 'Detail účtenky';
            $this->text = '';
            $this->keywords = 'detail, účtenka, účet';
        }
        
        if ($page == PAGE_BILL_EDIT)
        {
            $this->title = 'Editace účtenky na ' . conf::$server_name;
            $this->description = 'Editace účtenky';
            $this->text = '';
            $this->keywords = 'editace, účtenka, účet';
        }
        
        if ($page == PAGE_COMPANY)
        {
            $this->title = 'Editace firem na ' . conf::$server_name;
            $this->description = 'Editace firem';
            $this->text = '';
            $this->keywords = 'editace, firma';
        }
        
        if ($page == PAGE_SETTINGS)
        {
            $this->title = 'Nastavení účtu na ' . conf::$server_name;
            $this->description = 'Nastavení účtu';
            $this->text = '';
            $this->keywords = 'nastavení, účet';
        }

        
//        echo $this->title . ' '. $this->description;
        $r = array( 'title' => $this->title, 'description' => $this->description, 'text' => $this->text, 'keywords' => $this->keywords);
//        echo $r['title'] . ' '. $r['description'];
        return $r;
    }
    
}
?>