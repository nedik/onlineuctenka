<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'Metadata.php';
require_once 'Classes/User.php';
require_once 'Classes/Bill.php';
require_once 'Classes/Company.php';

/**
 * Router to control redirection in system
 */
class Router 
{

    private static $instance = NULL;

    
    /**
     * Constructor
     */
    function __construct() {
        ;
    }
    
    /**
     * Destructor
     */
    function __destruct() {
        ;
    }
    
    /**
     * Return instance of class
     * @return instance of class Router
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) {
            self::$instance = new $class;
        }
        return self::$instance;
    }

    /**
     * Routing by request 
     * @param User $user    class of User
     * @param Parser $parser class of Parser
     * @return null|string  array of info 
     */
    public function init($user, $parser) 
    {   
        $errors = Errors::get_instance();
//        $errors->add( ERROR_TEST,$_GET['url']);       
        
        if (isset($_POST['submit'])) 
        {
//            $errors->add( ERROR_TEST, 'submit ' . $_POST['submit']);
            
            if ($_POST['submit'] == SUBMIT_LOGIN) 
            {    
                if ($user->login())
                {
                    header("Location: " . HTTP . ACTION_LIST);
                    exit;
                }
                else {
                    header("Location: " . HTTP . ACTION_LOGIN);
                    exit;
                }
            }
            if ($_POST['submit'] == SUBMIT_REG) 
            { 
                if ($user->registration())
                {
                    header("Location: " . HTTP . ACTION_LIST);
                    exit;
                }
                else {
                    header("Location: " . HTTP . ACTION_LOGIN);
                    exit;
                }
            }
            if ($_POST['submit'] == SUBMIT_CREATE) 
            {
                if ($user->is_logged())
                {
                    $bill = Bill::get_instance();
                    $bill->create_bill($user);
                    return NULL;
                }
                else {
                    header("Location: " . HTTP . ACTION_FRONT);
                    exit;
                }
            }
            if ($_POST['submit'] == SUBMIT_EDIT) 
            {
                if ($user->is_logged())
                {
                    $bill = Bill::get_instance();
                    $bill->edit_bill($user);
                    return NULL;
                }
                else {
                    header("Location: " . HTTP . ACTION_FRONT);
                    exit;
                }
            }    
            
            if ($_POST['submit'] == SUBMIT_COMPANY) 
            {
                if ($user->is_logged())
                {
                    $comp = Company::get_instance();
                    $comp->create_company($user);
                    
                    return NULL;
                }
                else {
                    header("Location: " . HTTP . ACTION_FRONT);
                    exit;
                }
            } 
            
            if ($_POST['submit'] == SUBMIT_SEARCH)    
            {   
                if ($user->is_logged())
                {
                    header("Location: " . HTTP . ACTION_SEARCH . '?q=' . $_POST['search']);
                    return NULL;
                }
                else {
                    header("Location: " . HTTP . ACTION_FRONT);
                    exit;
                }
            }
            
            if ($_POST['submit'] == SUBMIT_SETTINGS) 
            {
                if ($user->is_logged())
                {
                    $user->change_pass();
                    
                    return NULL;
                }
                else {
                    header("Location: " . HTTP . ACTION_FRONT);
                    exit;
                }
            }
        }
        
        if (isset($_GET['submit'])) 
        {
//            $errors->add( ERROR_TEST, 'submit get ' . $_GET['submit']);
            if ($_GET['submit'] == SUBMIT_LOGOUT)    
            {   
                $user->logout();
                return NULL;
            }
            
            if ($_GET['submit'] == SUBMIT_DELETE)    
            {   
                if ($user->is_logged())
                {
                    $bill = Bill::get_instance();
                    $bill->delete_bill($user);
                    return NULL;
                }
                else {
                    header("Location: " . HTTP . ACTION_FRONT);
                    exit;
                }
            }
            if ($_GET['submit'] == SUBMIT_COMPANY_DELETE)    
            {   
                if ($user->is_logged())
                {
                    $comp = Company::get_instance();
                    $comp->delete_company($user);                    
                    return NULL;
                }
                else {
                    header("Location: " . HTTP . ACTION_FRONT);
                    exit;
                }
            }
            
        }
    
//        $errors->add( ERROR_TEST, 'test action');
        if (!is_null($parser->get_action()))
        {               
            $errors->add( ERROR_TEST, 'action ' . $parser->get_action());
            $metadata = Metadata::get_instance();
            $data = NULL;
            
            $action = $parser->get_action();
            
            if ($action == ACTION_FRONT)
            {   
                $data['page'] = PAGE_FRONT;
                $data['meta'] = $metadata->generate_metatag(PAGE_FRONT);
                $data['header'] = HEADER_1;
                $data['footer'] = FOOTER;
                $data['css'] = STYLE_FRONT;
                return $data;
            }
            
            if ($action == ACTION_LOGIN)
            {
                $data['page'] = PAGE_LOGIN;
                $data['meta'] = $metadata->generate_metatag(PAGE_LOGIN);
                $data['header'] = HEADER_1;
                $data['footer'] = FOOTER;
                $data['css'] = STYLE_LOGIN;
                return $data;
            }    

            if ($action == ACTION_REG)
            {
                $data['page'] = PAGE_REG;
                $data['meta'] = $metadata->generate_metatag(PAGE_REG);
                $data['header'] = HEADER_1;
                $data['footer'] = FOOTER;
                $data['css'] = STYLE_REGISTRATION;
                return $data;
            }
            
            if ($action == ACTION_COMPANY)
            {
                if ($user->is_logged())
                {
                    $data['page'] = PAGE_COMPANY;
                    $data['meta'] = $metadata->generate_metatag(PAGE_MAIN);
                    $data['header'] = HEADER;
                    $data['footer'] = FOOTER;
                    $data['css'] = STYLE_COMPANY;
                    return $data;
                }
                else {
                    header("Location: " . HTTP . ACTION_FRONT);
                    exit;
                }
            }
            
            if ($action == ACTION_LIST)
            {
                if ($user->is_logged())
                {
                    $data['page'] = PAGE_MAIN;
                    $data['meta'] = $metadata->generate_metatag(PAGE_MAIN);
                    $data['header'] = HEADER;
                    $data['footer'] = FOOTER;
                    $data['css'] = STYLE_MAIN;
                    return $data;
                }
                else {
                    header("Location: " . HTTP . ACTION_FRONT);
                    exit;
                }
            }
            
            if ($action == ACTION_SEARCH)
            {
                if ($user->is_logged())
                {
                    $data['page'] = PAGE_MAIN;
                    $data['meta'] = $metadata->generate_metatag(PAGE_MAIN);
                    $data['header'] = HEADER;
                    $data['footer'] = FOOTER;
                    $data['css'] = STYLE_MAIN;
                    return $data;
                }
                else {
                    header("Location: " . HTTP . ACTION_FRONT);
                    exit;
                }
            }
            
            if ($action == ACTION_BILL_CREATE)
            {
                if ($user->is_logged())
                {
                    $data['page'] = PAGE_BILL_CREATE;
                    $data['meta'] = $metadata->generate_metatag(PAGE_BILL_CREATE);
                    $data['header'] = HEADER;
                    $data['footer'] = FOOTER;
                    $data['css'] = STYLE_CREATE_EDIT;
                    return $data;
                }
                else {
                    header("Location: " . HTTP . ACTION_FRONT);
                    exit;
                }
            }
            
            if ($action == ACTION_BILL_DETAIL)
            {
                if ($user->is_logged())
                {
                    $data['page'] = PAGE_BILL_DETAIL;
                    $data['meta'] = $metadata->generate_metatag(PAGE_BILL_DETAIL);
                    $data['header'] = HEADER;
                    $data['footer'] = FOOTER;
                    $data['css'] = STYLE_DETAIL;
                    return $data;
                }
                else {
                    header("Location: " . HTTP . ACTION_LOGIN);
                    exit;
                }
            }
            
            if ($action == ACTION_BILL_EDIT)
            {
                if ($user->is_logged())
                {
                    $data['page'] = PAGE_BILL_EDIT;
                    $data['meta'] = $metadata->generate_metatag(ACTION_BILL_EDIT);
                    $data['header'] = HEADER;
                    $data['footer'] = FOOTER;
                    $data['css'] = STYLE_CREATE_EDIT;
                    return $data;
                }
                else {
                    header("Location: " . HTTP . ACTION_LOGIN);
                    exit;
                }
            }
            
            if ($action == ACTION_SETTINGS)
            {
                if ($user->is_logged())
                {
                    $data['page'] = PAGE_SETTINGS;
                    $data['meta'] = $metadata->generate_metatag(PAGE_MAIN);
                    $data['header'] = HEADER;
                    $data['footer'] = FOOTER;
                    $data['css'] = STYLE_SETTINGS;
                    return $data;
                }
                else {
                    header("Location: " . HTTP . ACTION_LOGIN);
                    exit;
                }
            }
            
            if ($action == ACTION_CONTACTS)
            {
                if ($user->is_logged())
                {
                    $data['page'] = PAGE_CONTACTS;
                    $data['meta'] = $metadata->generate_metatag(PAGE_CONTACTS);
                    $data['header'] = HEADER;
                    $data['footer'] = FOOTER;
                    $data['css'] = STYLE_CONTACTS;
                    return $data;
                }
                else {
                    header("Location: " . HTTP . ACTION_LOGIN);
                    exit;
                }
            }
            
            if ($action == ACTION_CONTACTS_FRONT)
            {
                $data['page'] = PAGE_CONTACTS;
                $data['meta'] = $metadata->generate_metatag(PAGE_CONTACTS);
                $data['header'] = HEADER_1;
                $data['footer'] = FOOTER;
                $data['css'] = STYLE_CONTACTS;
                return $data;             
            }
        }
        
        
        if ($user->is_logged())
        {
            header("Location: " . HTTP . ACTION_LIST);
            exit;
        }
        else {
            header("Location: " . HTTP . ACTION_FRONT);
            exit;
        }
    }
}


?>
