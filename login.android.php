<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    require_once 'Notification.php';
    require_once 'Logger.php';
    require_once 'Errors.php';
    require_once 'Classes/User.php';
    require_once 'Database/DB_Category.php';
    
    session_start();

    $response = array();
 
    $logger = Logger::get_instance();    
    $error = Errors::get_instance();
 

    if (isset($_POST['submit']))
    {
        if ($_POST['submit'] == SUBMIT_LOGIN)
        {
            if (!$_POST['email'] || !$_POST['password']) 
            {
                $response["success"] = 0;
                $response["message"] = $notifi->get_str();

                echo json_encode($response);
            }
            
            $email = $_POST['email'];
            $password = $_POST['password'];
            
            $user = User::get_instance();
            $login = $user->login();

            if ($login) 
            {    
                $response["success"] = 1;
                $response["id"] = $user->__get('id');
                $response["email"] = $user->__get('email');;

                echo json_encode($response);
                exit;
            } 
        }
        
        if ($_POST['submit'] == 'get_cat')
        {               
//            $cat = array();
            
            $cat = category();                       
            
            $response["success"] = 1;
            $response["categoryies"] = $cat;
            
            echo json_encode($response);
            exit;
        }
        
        if ($_POST['submit'] == 'get_sel_cat')
        {               
//            $cat = array();
            
            $cat = category_sel();                       
            
            $response["success"] = 1;
            $response["categoryies"] = $cat;
            
            echo json_encode($response);
            exit;
        }
        
        if ($_POST['submit'] == 'get_comp')
        {      
            
            $id = $_POST['id_user'];
            
            $cat = company($id);
            
            $response["success"] = 1;
            $response["companyies"] = $cat;
            
            echo json_encode($response);
            exit;
        }
        
        if ($_POST['submit'] == 'set_comp')
        {                  
            $id = $_POST['id_user'];
            $name = $_POST['name'];
            
            $res = new_company($id, $name);
            
            if ($res)
            {
            
                $response["success"] = 1;
            
                echo json_encode($response);
                exit;
            }
            else {
                $response["success"] = 0;
                $response["message"] = 'insert new company failed, company already exists';

                echo json_encode($response);   
    
                exit;
            }
        }
        
        if ($_POST['submit'] == 'save')
        {   
            $bill = Bill::get_instance();
            
            $id = $_POST['id_user'];
            
            $res = $bill->create_bill_android($id);
            
            if ($res)
            {            
                $response["success"] = 1;
            
                echo json_encode($response);
                exit;
            }
        }
    }
    
    $notifi = Notification::get_instance();
    
    $response["success"] = 0;
    $response["message"] = $notifi->get_str_text();

    echo json_encode($response);   
    
    exit;
 
    
    
//    $logger->add( time(), ERROR_CONNECTION, debug_backtrace(), 'Login ' . $email . ' pass ' . $password  . '<br>');
    
    /**
     * Create tree list of categories
     * @param uint $id id of selected category
     * @return type
     */
    function category_sel()
    {
        $db = Database::get_instance();
        $db->connect();

        $id = $_POST['id_parent'];
        
        $cat = array();
        
        $category = DB_Category::get_instance();
        $data = $category->get_categories_parent($db, id_parent);
                
        
        $cat[] = print_r($data, true);
        
        while ($row = $db->fetch_array($data)) 
        {                        
            $cat[] = array( 'id' => $row['id'], 'name' => $row['name']);
        }

        $db->disconnect();
        
        return $cat;
    }
    
    /**
     * Create tree list of categories
     * @param uint $id id of selected category
     * @return type
     */
    function category()
    {
        $db = Database::get_instance();
        $db->connect();

        $cat = array();
        
        $category = DB_Category::get_instance();
        $data = $category->get_categories_root($db);
                
        
        //$cat[] = print_r($data, true);
        
        while ($row = $db->fetch_array($data)) 
        {                        
            $cat[] = array( 'id' => $row['id'], 'name' => $row['name'], 
                'sub' => get_item($db, $category, 1, $row['category_id']));
        }

        $db->disconnect();
        
        return $cat;
    }
    
    /**
     * Create tree list of categories
     * @param uint $id id of selected category
     * @return type
     */
    function company($id)
    {
        $db = Database::get_instance();
        $db->connect();

        $cat = array();
        
        $category = DB_Company::get_instance();
        $data = $category->get_companyies($db, $id);
                
        
        //$cat[] = print_r($data, true);
        
        while ($row = $db->fetch_array($data)) 
        {                        
            $cat[] = array( 'id' => $row['id'], 'name' => $row['name']);
        }

        $db->disconnect();
        
        return $cat;
    }
    
    function new_company($id, $name)
    {
        $db = Database::get_instance();
        $db->connect();

        $cat = array();
        
        $category = DB_Company::get_instance();
        $res = $category->insert($db, $id, $name);

        $db->disconnect();
        
        return $res;
    }

    /**
     * Create tree list of subcategories for caterory
     * @param Database  $db         Database
     * @param Category  $category   Category class
     * @param uint      $level      level
     * @param uint      $parent_id  parent id
     * @param uint      $id         id of selested item
     */
    function get_item( $db, $category, $level, $parent_id) 
    {
        $data = $category->get_categories_parent($db, $parent_id);

        $cat = array();
        
//        $s = '';
//        for ($i=1; $i<=$level; $i++)
//        { 
//            $s .= '  '; 
//        }

        while ($row = $db->fetch_array($data)) 
        {
            $cat[] = array( 'id' => $row['id'], 'name' => $row['name'], 
                'sub' => get_item($db, $category, $level+1, $row['category_id']));
        }
        
        return $cat;
    }
    
?>
