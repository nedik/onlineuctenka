<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    //notification
    
    define("NOTIFI_NORMAL", 'normal');
    define("NOTIFI_ERROR", 'error');    

    //errors
    /** Error testing*/
    define("ERROR_TEST", 1);
    /** Error input parameters */
    define("ERROR_INPUT", 101);
    
    /** Error database */
    define("ERROR_DATBASE", 200);
    /** Error connect database */
    define("ERROR_CONNECTION", 201);
    /** Error realize query to database */
    define("ERROR_QUERY", 202);
    /** Error realize select query to database */
    define("ERROR_SELECT", 203);
    /** Error realize insert query to database */
    define("ERROR_INSERT", 204);
    /** Error realize update query to database */
    define("ERROR_UPDATE", 205);
    /** Error realize dlete query to database */
    define("ERROR_DELETE", 206);
    /** Error id user of selection is not same as logged user to database */
    define("ERROR_ID", 207);
    
    /** Error to login user to system */
    define("ERROR_LOGIN", 301);
    /** Error user don't fill all columbs to login */
    define("ERROR_LOGIN_DATA", 302);
    /** Error user set wrong password to login */
    define("ERROR_LOGIN_PASS", 303);
    
    /** Error to registrate user to system */
    define("ERROR_REGISTR", 304);
    /** Error user don't fill all columbs to registrate */
    define("ERROR_REGISTR_DATA", 305);
    /** Error user set wrong passwords to registrate */
    define("ERROR_REGISTR_PASS", 306);
    /** Error user set wrong emial to registrate, email aleady exists */
    define("ERROR_REGISTR_EXIST", 307);
    
    /** Error user don't send all data bill */
    define("ERROR_BILL_DATA", 304);
    
    /** Error working with image */
    define("ERROR_IMAGE", 310);
    /** Error insert image to system */
    define("ERROR_INSERT_IMAGE", 311);
    
    // actions
    /** Action to load front page */
    define("ACTION_FRONT", 'front');
    /** Action login user to system */
    define("ACTION_LOGIN", 'login');
    /** Action registrate user to system */
    define("ACTION_REG", 'registration');
    /** Action go to main page */
//    define("ACTION_MAIN", 'main');
    /** Action to create new bill in system */
    define("ACTION_BILL_CREATE", 'bill_create');
    /** Action to get detail of bill in system */
    define("ACTION_BILL_DETAIL", 'bill_detail');
    /** Action to edit bill in system */
    define("ACTION_BILL_EDIT", 'bill_edit');
    /** Action to reload list of categories */
    define("ACTION_LIST", 'bill_list');
    
    /** Action to reload list of categories */
    define("ACTION_SEARCH", 'bill_search');
    
    /** Action to reload list of categories */
    define("ACTION_COMPANY", 'companyies_list');
    /** Action to reload list of categories */
    define("ACTION_SETTINGS", 'sett');
    /** Action to reload list of categories */
    define("ACTION_CONTACTS", 'contacts');
    /** Action to reload list of categories */
    define("ACTION_CONTACTS_FRONT", 'contacts_front');
    
    // submit
    /** Submit to login user */
    define("SUBMIT_LOGIN", "login");
    /** Submit to registration of user */
    define("SUBMIT_REG", "registration");
    /** Submit to create new bill */
    define("SUBMIT_CREATE", "create");
    /** Submit to edit selected bill */
    define("SUBMIT_EDIT", "edit");
    /** Submit to delete selected bill */
    define("SUBMIT_DELETE", "delete");
    /** Submit to logout user */
    define("SUBMIT_LOGOUT", 'logout');
    /** Submit to load main page */
    define("SUBMIT_MAIN", "main");    
    /** Submit to search */
    define("SUBMIT_COMPANY", "company");
    /** Submit to delete selected bill */
    define("SUBMIT_COMPANY_DELETE", "delete_company");
    
    /** Submit to search */
    define("SUBMIT_SEARCH", "search");
    /** Submit to search */
    define("SUBMIT_SETTINGS", "settings");
    
    
    //orders
    /** Order by columb */
    define("ORDER", "order");
    /** Order by name */
    define("ORDER_BY", "by");
    /** Order by name */
    define("ORDER_BY_NAME", "name");
    /** Order by company */
    define("ORDER_BY_COMPANY", "company");
    /** Order by date */
    define("ORDER_BY_DATE", "date");
    /** Order ASC */
    define("ORDER_ASC", "asc");
    /** Order DESC */
    define("ORDER_DESC", "desc");
    
    //lists
    define("ACTUAL_PAGE", "page");
    define("ON_PAGE", 5);
    
    // page
    /** Page to login user */
    define("PAGE_FRONT", 'Pages/front.php');
    
    /** Page with header */
    define("HEADER", 'Pages/header.php');
    /** Page with header */
    define("HEADER_1", 'Pages/header_1.php');
    /** Page with footer */
    define("FOOTER", 'Pages/footer.php');
    
    /** Page to login user */
    define("PAGE_LOGIN", 'Pages/login.php');
    /** Page to registrate user */
    define("PAGE_REG", 'Pages/registration.php');
    /** Page with main menu */
    define("PAGE_MAIN", 'Pages/main.php');
    /** Page to see detail of selected bill */
    define("PAGE_BILL_DETAIL", 'Pages/bill/detail.php');
    /** Page to create new bill */
    define("PAGE_BILL_CREATE", 'Pages/bill/create-edit.php');
    /** Page to edit selected bill */
    define("PAGE_BILL_EDIT", 'Pages/bill/create-edit.php');
    
    /** Page to edit selected bill */
    define("PAGE_BILL_SIMILAR", 'Pages/bill/similar_bills.php');
    /** Page to edit selected bill */
    define("PAGE_BILLS", 'Pages/main/bills.php');
    /** Page to edit selected bill */
    define("PAGE_CATEGORY_LIST", 'Pages/main/category_list.php');
    
    /** Page to edit selected bill */
    define("PAGE_COMPANY", 'Pages/company.php');
    
    /** Page to edit selected bill */
    define("PAGE_SETTINGS", 'Pages/settings.php');
    
    /** Page to edit selected bill */
    define("PAGE_ERROR", 'Pages/404.php');
    /** Page to edit selected bill */
    define("PAGE_CONTACTS", 'Pages/contacts.php');
    
    /** Name of aplication */
    define("Name_of_application", 'onlineUctenka.apk');
    
    
    // styles
    /** Definition of style */
    define("STYLE_FRONT", 'css/front.css');
    /** Definition of style */
    define("STYLE_LOGIN", 'css/login.css');
    /** Definition of style */
    define("STYLE_REGISTRATION", 'css/login.css');
    /** Definition of style */
    define("STYLE_MAIN", 'css/main.css');
    /** Definition of style */
    define("STYLE_DETAIL", 'css/detail.css');
    /** Definition of style */
    define("STYLE_CREATE_EDIT", 'css/create-edit.css');
    /** Definition of style */
    define("STYLE_COMPANY", 'css/company.css');
    
    /** Definition of style */
    define("STYLE_SETTINGS", 'css/settings.css');
    /** Definition of style */
    define("STYLE_CONTACTS", 'css/contacts.css');
?>
