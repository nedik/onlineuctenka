<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//88.86.105.71
define("HTTP",'/Project_1/');

class conf
{
    /** Name of log file */
    static $log = 'log.txt';
    
    /** Name of webside */
    static $server_name = 'Bills database';
    /** Webside description */
    static $server_description = 'Server to save yours bills and controling their assurance';

    /** address of server */
    static $database = 'localhost';
    /** database name  */
    static $db_name = 'db_bills';
    /** username to connect to database */
    static $db_username = 'root';
    /** password to connect to database */
    static $db_password = 'root';
    
    /** password to connect ot database */
    static $pass_salt = 'heslo';
    
    /** current directory in server */
    static $path = __DIR__;
    /** directory from files in server */
    static $directory = 'files';
    /** directory from files in server alternative*/
    static $directory_alt = '/../files/';
    
    /**
     * Get directory to save files
     * @return type
     */
    public static function get_path()
    {
        $path = self::$path . self::$directory_alt;
        $path = realpath($path);
        return $path; 
    }
    
    static $mail_subject = 'Registrace';
    static $mail_from_user = 'Admin onlineUctenka';
    //static $mail_from_email = 'admin@onlineuctenka.cz';
    static $mail_from_email = 'info@onlineuctenka.cz';

    //function mail_utf8($to, $from_user, $from_email, $subject = '(No subject)', $message = '')
    public static function get_mail( $email, $password)
    {
        $message = 'Dekujeme za vytvoreni uctu na portalu onlineUctenka.cz' . PHP_EOL;
        $message .= 'Vase prihlasovaci udaje' . PHP_EOL;
        $message .= '   email: ' . $email . PHP_EOL;
        $message .= '   heslo: ' . $password . PHP_EOL;
        $message .= PHP_EOL;
        $message .= 'S pozdravem admin' . PHP_EOL;
        
        return $message; 
    }
    
}




?>
