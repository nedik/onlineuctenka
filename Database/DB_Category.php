<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'Errors.php';

/**
 * Description of Category
 *
 * @author redak105
 */
class DB_Category
{
    private static $instance = NULL;

    /**
     * Constructor
     */
    function __construct() {
        ;
    }
    
    /**
     * Destructor
     */
    function __destruct() {
        ;
    }
    
    /**
     * Return instance of class
     * @return instance of class DB_Category
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) {
            self::$instance = new $class;
        }
        return self::$instance;
    }
    
    /**
     * Insert new category
     * @param mysqli    $database       database
     * @param uint      $parent_id      id of parent
     * @param uint      $category_id    category id
     * @param string    $name           name of category
     * @param string    $url            url of category
     * @return boolean result
     */
    public function insert( $database, $parent_id, $category_id, $name, $url)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$category_id || !$name)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in insert category');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get catalogs');
            return FALSE;
        }
        
        $query = 'SELECT c.category_id FROM category c WHERE c.category_id = ' . $category_id;
        $result = $database->query($query);
        $row = mysqli_fetch_assoc($result);
        if ($row)
        {                
            $errors->add( ERROR_REGISTR_EXIST, 'Category id exists in insert category');
            return FALSE;
        }
        
        if ($parent_id)
        {
            $query = 'SELECT c.category_id FROM category c WHERE c.category_id = ' . $parent_id ;
            if (!$database->query($query))
            {
                $errors->add( ERROR_SELECT, 'Parent id doesnt exist id dataase');
                return FALSE;
            }
        } 
        
        
        if ($parent_id)
        {
            $query = 'INSERT INTO category( id, parent_id, category_id, name, url) VALUES ( NULL, ' . $parent_id . ', ' . $category_id . ', "' . addslashes($name) . '", "' . addslashes($url) .'");';
        }
        else {
            $query = 'INSERT INTO category( id, parent_id, category_id, name, url) VALUES ( NULL, 0, ' . $category_id . ', "' . addslashes($name) . '", "' . addslashes($url) . '");';
        }    

       // $query = 'INSERT INTO category( id, id_parent, category_id, name) VALUES ( NULL, ' . $id_parent . ', ' . $category_id . ', "' . addslashes($name) . '");';
       
        $result = $database->query($query);
        if (!$result)
        {
            $errors->add( ERROR_INSERT, 'Insert new category');
            $logger->add( time(), ERROR_INSERT, debug_backtrace(), 'Insert new category ' . $query);
            return FALSE;
        }
        return TRUE;
    }
    
    /**
     * Select categories
     * @param mysqli    $database   database
     * @return null|string result
     */
    public function get_categories( $database)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in get categories');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get categories');
            return NULL;
        }
        
        $query = 'SELECT c.id, c.parent_id, c.category_id, c.name FROM category c';
                
        $result = $database->query($query);
        if (!$result)
        {
            $errors->add( ERROR_SELECT, 'Categories selection');
            $logger->add( time(), ERROR_INSERT, debug_backtrace(), 'Insert new category ' . $query);
            return NULL;
        }
        
        return $result;
    }
    
    
    /**
     * Select Root categories
     * @param mysqli $database  database
     * @return null|string result
     */
    public function get_categories_root( $database)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
         
        if (!$database)
        {
            $errors->add( ERROR_INPUT,'ERROR input parameters in get categories');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get categories');
            return NULL;
        }
        
        $query = 'SELECT c.id, c.parent_id, c.category_id, c.name FROM category c WHERE c.parent_id = 0';
                
        $result = $database->query($query);
        if (!$result)
        {
            $errors->add( ERROR_SELECT, 'Categories selection');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Categories selection ' . $query);
            return NULL;
        }
        
        return $result;
    }
    
    /** 
     * Select categories with this parent 
     * @param mysqli    $database   database
     * @param uint      $parent_id  parent id
     * @return null|string result
     */
    public function get_categories_parent( $database, $parent_id)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database)
        {
            $errors->add( ERROR_INPUT,'ERROR input parameters in get categories');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get categories');
            return NULL;
        }
        
        $query = 'SELECT c.id, c.parent_id, c.category_id, c.name FROM category c WHERE c.parent_id = ' . $parent_id;
                
        $result = $database->query($query);
        if (!$result)
        {
            $errors->add( ERROR_SELECT, 'Categories selection');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Categories selection ' . $query);
            return NULL;
        }
        
        return $result;
    }
    
    
    /**
     * Select category from url
     * @param mysqli    $database       database
     * @param uint      $url            url of category
     * @return null|string result
     */
    public function get_category( $database, $url)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database)
        {
            $errors->add( ERROR_INPUT,'ERROR input parameters in get categories');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get categories');
            return NULL;
        }
        
        $query = 'SELECT c.id, c.parent_id, c.category_id, c.name, c.url FROM category c WHERE c.url = "' . addslashes($url) . '"';
                
        $result = $database->query($query);
        if (!$result)
        {
            $errors->add( ERROR_SELECT, 'Categories selection');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Categories selection ' . $query);
            return NULL;
        }
        
        return $result;
    }
    
    /**
     * Select Root categories
     * @param mysqli $database  database
     * @param uint      $parent_id  parent id
     * @return null|string result
     */
    public function get_parent( $database, $parent_id)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in get parrent category');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get parrent category');
            return NULL;
        }
        
        $query = 'SELECT c.id, c.parent_id, c.category_id, c.name FROM category c WHERE c.category_id = ' . $parent_id;
        $result = $database->query($query);
        if (!$result)
        {
            $errors->add( ERROR_SELECT, 'Categories selection');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Categories selection ' . $query);
            return NULL;
        }
        
        return $result;
    }
    
}

?>
