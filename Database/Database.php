<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('Errors.php');

class Database
{
    private static $instance = NULL;
    /** database connector */
    private $mysqli = NULL;
    /** variables used to connect */
    private $db_host, $db_user, $db_password, $db_database;
    
    
    /**
     * Constructor
     * @param string $db_host       addres of host
     * @param string $db_user       username
     * @param string $db_password   password for user
     * @param string $db_database   name of database
     */
    public function __construct( $db_host = NULL, $db_user  = NULL, $db_password = NULL, $db_database = NULL)
    {
        if (is_null($db_host))
        {
            $db_host = conf::$database;
        }
        $this->db_host = $db_host;
        
        if (is_null($db_user))
        {
            $db_user = conf::$db_username;
        }
        $this->db_user = $db_user;
        
        if (is_null($db_password))
        {
            $db_password = conf::$db_password;
        }
        $this->db_password = $db_password;
        
        if (is_null($db_database))
        {
            $db_database = conf::$db_name;
        }
        $this->db_database = $db_database;
        
        $this->connect();
    }
    
    /**
     * Destructor
     */
    public function __destruct()
    {
        $this->disconnect();
    }
    
    /**
     * Return instance of class
     * @param Database $instance    instance of class
     * @param string $db_host       addres of host
     * @param string $db_user       username
     * @param string $db_password   password for user
     * @param string $db_database   name of database
     * @return instance of class
     */
    public static function get_instance($instance = 'default', $db_host = NULL, $db_user  = NULL, $db_password = NULL, $db_database = NULL) 
    {
        $class = __CLASS__;
        if (self::$instance[$instance] == NULL) 
        {
            self::$instance[$instance] = new $class( $db_host, $db_user, $db_password, $db_database);
        }
        
        return self::$instance[$instance];        
    }
    
    /**
     * Connect to database
     * @return type
     */
    public function connect()
    {
        return $this->connect_database($this->db_host, $this->db_user, $this->db_password, $this->db_database);
    }

    /**
     * Connection to database
     * @param string $db_host       addres of host
     * @param string $db_user       username
     * @param string $db_password   password for user
     * @param string $db_database   name of database
     * @return boolean succes of connection
     */
    private function connect_database( $db_host, $db_user, $db_password, $db_database)
    {       
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if ($this->mysqli) 
        {
//            $errors->add( ERROR_CONNECTION, 'Database connection already exists');
            return TRUE;
        }        
        
        $this->mysqli = new mysqli( $db_host, $db_user, $db_password, $db_database);
        
        if ($this->mysqli->connect_errno) 
        {   
            $errors->add( ERROR_CONNECTION, 'Failed to connect to MySQL: (' . $this->mysqli->connect_errno . ') ' . $this->mysqli->connect_error  . '<br>');
            $logger->add( time(), ERROR_CONNECTION, debug_backtrace(), 'Failed to connect to MySQL: (' . $this->mysqli->connect_errno . ') ' . $this->mysqli->connect_error  . '<br>');
            return FALSE;
        }
        
        $this->mysqli->query('SET NAMES UTF8');
        $this->mysqli->query('SET CHARACTER SET UTF8');
                
        return TRUE;
    }

    /**
     * Disconect from database
     */
    public function disconnect()
    {
        if (isset($this->mysqli))
        {
            mysqli_close($this->mysqli);
            $this->mysqli = NULL;
        }
    }
    
    
    /**
     * Run query in database
     * @param string    $query  query to realize
     * @return null|string   result
     */
    public function query($query)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$this->mysqli)
        {
            $errors->add( ERROR_DATBASE, 'Database is not connected');
            return NULL;
        }
        
        $res = $this->mysqli->query($query);
        if (!$res)
        {
            $errors->add( ERROR_QUERY, '(' . $this->mysqli->errno . ') ' . $this->mysqli->error . " " . $query);
            $logger->add( time(), ERROR_QUERY, debug_backtrace() , '(' . $this->mysqli->errno . ') ' . $this->mysqli->error . " " . $query);
            
            return NULL;
        }
        
        return $res;
    }
    
    /**
     * Convert result from database to arrat
     * @param result $result result of query to database
     * @return array    
     */
    public function fetch_array( $result)
    {
        $array = mysqli_fetch_array($result, MYSQLI_ASSOC);
        
        return $array;
    }
    
    /**
     * Get id of last inser query to database
     * @return null
     */
    public function get_insert_id()
    {
        if ($this->mysqli)
        {
            return mysqli_insert_id($this->mysqli);
        }
        
        $errors = Errors::get_instance();
        $errors->add( ERROR_CONNECTION, 'You are not connected');
        
        return NULL;
    }
    
    /**
     * Get numbers of rows from result
     * @param result $result result of query to database
     * @return null|int number of rows
     */
    public function get_num_rows($result)
    {
        if ($this->mysqli)
        {
            return mysqli_num_rows($result);
        }
        
        $errors = Errors::get_instance();
        $errors->add( ERROR_CONNECTION, 'You are not connected');
        
        return 0;
    }
}

?>
