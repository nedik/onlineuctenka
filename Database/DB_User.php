<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('Errors.php');
require_once('Settings/conf.php');

/**
 * Description of User
 *
 * @author redak105
 */
class DB_User 
{
    private static $instance = NULL;

    /**
     * Constructor
     */
    function __construct() {
        ;
    }
    
    /**
     * Destructor
     */
    function __destruct() {
        ;
    }
    
    /**
     * Return instance of class
     * @return instance of class DB_User
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) {
            self::$instance = new $class;
        }
        return self::$instance;
    }
    
    /**
     * Insert new user
     * @param mysqli    $database   database
     * @param string    $email      email of user
     * @param string    $password   password of user
     * @return boolean result
     */
    public function insert( $database, $email, $password)
    {
        $errors = Errors::get_instance();
        $notifi = Notification::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$email || !$password)
        {
            $errors->add( ERROR_INPUT, 'Input parameters to insert user ');            
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in insert to bill');
            return FALSE;
        }
        
        $query = 'SELECT u.id FROM user u WHERE u.email = "' . addslashes($email) . '"';
        $result = $database->query($query);
        
        if ($database->get_num_rows($result) != 0)
        {                
            $errors->add( ERROR_REGISTR_EXIST, 'Email already exists');
            $notifi->add(NOTIFI_NORMAL, 'tento email už je zaregistrovaný');
            return FALSE;
        }
        
        $password_md5 = $password . conf::$pass_salt;
        $password_md5 = md5($password_md5);
        
        
        $query = 'INSERT INTO user( id, email, password) VALUES ( NULL, "' . addslashes($email) . '", "' . addslashes($password_md5) . '");';
        if (!$database->query($query))
        {
            $errors->add( ERROR_INSERT, 'Insert new user');
            $logger->add( time(), ERROR_INSERT, debug_backtrace(), 'Insert new user ' . $query);
            return FALSE;
        }
        return TRUE;
    }
    
    
    /**
     * Get data of user
     * @param mysqli    $database   database
     * @param string    $email      email of user     
     * @return boolean
     */
    function get_user( $database, $email)
    {   
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$email)
        {
            $errors->add( ERROR_INPUT, 'Inputs parameters to select user');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Inputs parameters to select user');
            return NULL;
        }
        
        $query = 'SELECT u.id, u.email , u.password FROM user u WHERE u.email = "' . addslashes($email) . '"';
        $result = $database->query($query);
        $row = mysqli_fetch_assoc($result);
        if (!$row)
        {          
            $errors->add( ERROR_SELECT, 'Selection user in login');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Selection user in login');            
            return NULL;
        }
       
        return $row;
    } 
    
    function update_pass( $database, $id_user, $password)
    {   
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$id_user || !$password)
        {
            $errors->add( ERROR_INPUT, 'Inputs parameters to update password');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Inputs parameters to update password');
            return FALSE;
        }
        
        $password_md5 = $password . conf::$pass_salt;
        $password_md5 = md5($password_md5);
        
        $query = 'UPDATE user SET password="' . addslashes($password_md5) . '" WHERE id=' . $id_user;  
        
        $result = $database->query($query);
        
        if (!$result)
        {          
            $errors->add( ERROR_SELECT, 'Update user password');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Update user password');            
            return FALSE;
        }
       
        return TRUE;
    } 
    
    
    
}

?>
