<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'Errors.php';

/**
 * Description of Bill
 *
 * @author redak105
 */
class DB_Bill
{
    private static $instance = NULL;
    
    /**
     * Constructor
     */
    function __construct() {
        ;
    }
    
    /**
     * Destructor
     */
    function __destruct() {
        ;
    }
    
    /**
     * Return instance of class
     * @return instance of class DB_Bill
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) {
            self::$instance = new $class;
        }
        return self::$instance;
    }
    
    /**
     * Insert new Bill
     * @param mysqli    $database       database
     * @param uint      $id_user        id of user
     * @param uint      $id_company     id of company
     * @param uint      $id_category    id of category
     * @param string    $name           name of product
     * @param date      $date           date of buy
     * @param int       $assurance      duration of assurance
     * @param string    $description    description of product
     * @param string    $file           address of bills image
     * @return boolean insert succes
     */
    public function insert( $database, $id_user, $id_company, $id_category, $name, $date, $assurance, $description, $file)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();        
        
        if (!$database || !$id_user || !$id_company || !$id_category || !$name || !$date || !$file)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in insert to bill');
            
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in insert to bill');
             
            return FALSE;
        }
        
        if ($id_user)
        {
            $query = 'SELECT u.id FROM user u WHERE u.id = ' . $id_user;
            $result = $database->query($query);
            $row = mysqli_fetch_assoc($result);
            if (!$row)
            {
                $errors->add( ERROR_SELECT, 'Id user doesnt exist');
                return FALSE;
            }
        } 
        
        if ($id_company)
        {
            $query = 'SELECT c.id FROM company c WHERE c.id = ' . $id_company;
            $result = $database->query($query);
            $row = mysqli_fetch_assoc($result);
            if (!$row)
            {
                $errors->add( ERROR_SELECT, 'Id company doesnt exist');
                return FALSE;
            }
        }
        
        if ($id_category)
        {
            $query = 'SELECT c.id FROM category c WHERE c.id = ' . $id_category;
            $result = $database->query($query);
            $row = mysqli_fetch_assoc($result);
            if (!$row)
            {
                $errors->add( ERROR_SELECT, 'Id category doesnt exist');
                return FALSE;
            }
        }
        
        //$query = 'SELECT b.id FROM bill b WHERE b.name = "' . addslashes($name) . '"';
       
        
        $url = $name;
        $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
        $url = trim($url, "-");
        $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
        $url = strtolower($url);
        $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
        
        
        
        $query = 'SELECT b.id FROM bill b WHERE b.url REGEXP "^' . $url . '_[0-9]+"';
        $result = $database->query($query);
        
        $count = 0;
        if (!is_null($result))
        {
            $count = $database->get_num_rows($result);
        }
        
        $url .= '_' . $count;
        
        $query = 'INSERT INTO bill( id, id_user, id_company, id_category, name, date, assurance, description, file, deleted, url) 
                    VALUES ( NULL, ' . $id_user . ', ' . $id_company . ', ' . $id_category . ', "' . addslashes($name)
                . '", "' . addslashes($date) . '", ' . $assurance . ', "' . addslashes($description) . '", "' . addslashes($file) . '", 0, "' . addslashes($url) . '");';
        
        if (!$database->query($query))
        {
            $errors->add( ERROR_INSERT, 'Insert new bill');
            $logger->add( time(), ERROR_INSERT, debug_backtrace(), 'Insert new bill ' . $query);
            
            return FALSE;
        }
        return TRUE;
    }
    
    /**
     * Updat existing Bill
     * @param mysqli    $database       database
     * @param uint      $id             id fo bill
     * @param uint      $id_user        id of user
     * @param uint      $id_company     id of company
     * @param uint      $id_category    id of category
     * @param string    $name           name of product
     * @param date      $date           date of buy
     * @param int       $assurance      duration of assurance
     * @param string    $description    description of product
     * @param string    $file           address of bills image
     * @return boolean  sucess of update
     */
    public function update( $database, $id, $id_user, $id_company, $id_category, $name, $date, $assurance, $description, $file)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$id || !$id_user || !$id_company || !$id_category || !$name || !$date || !$file)
        {
             $errors->add( ERROR_INPUT, 'Input parameters in insert to bill');
             
             $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in insert to bill');
             
             return FALSE;
        }
        
        if ($id_user)
        {
            $query = 'SELECT u.id FROM user u WHERE u.id = ' . $id_user;
            $result = $database->query($query);
            $row = mysqli_fetch_assoc($result);
            if (!$row)
            {
                $errors->add( ERROR_SELECT, 'Id user doesnt exist');
                return FALSE;
            }
        } 
        
        if ($id_company)
        {
            $query = 'SELECT c.id FROM company c WHERE c.id = ' . $id_company;
            $result = $database->query($query);
            $row = mysqli_fetch_assoc($result);
            if (!$row)
            {
                $errors->add( ERROR_SELECT, 'Id company doesnt exist');
                return FALSE;
            }
        }
        
        if ($id_category)
        {
            $query = 'SELECT c.id FROM category c WHERE c.id = ' . $id_category;
            $result = $database->query($query);
            $row = mysqli_fetch_assoc($result);
            if (!$row)
            {
                $errors->add( ERROR_SELECT, 'Id category doesnt exist');
                return FALSE;
            }
        }
        
        $query = 'UPDATE bill SET id_company=' . $id_company . ', id_category=' . $id_category . ', name="' . addslashes($name). 
                '", date="' . addslashes($date) . '", assurance=' . $assurance . ', description="' . addslashes($description) . '", file="' . addslashes($file) .
                '" WHERE id=' . $id;
        
        if (!$database->query($query))
        {
            $errors->add( ERROR_UPDATE, 'Update bill');
            $logger->add( time(), ERROR_UPDATE, debug_backtrace(), 'Update bill ' . $query);
            
            return FALSE;
        }
        return TRUE;
    }
    
    /**
     * Select bills belong to user fron category
     * @param myslqi    $database       database
     * @param uint      $id_user        id of user
     * @param uint      $id             url of category
     * @param string    $order          order by columb
     * @param string    $order_by       order position
     * @return null|string result
     */
    public function get_bills( $database, $id_user, $id = NULL, $order = NULL, $order_by = NULL, $search = NULL, $start = NULL, $limit = NULL)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$id_user)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in get bills for user');
            $logger->add( time(), ERROR_UPDATE, debug_backtrace(), 'Update bill ' . $query);
            
            return NULL;
        }
        
        $query = 'SELECT b.id, b.id_user, b.id_company, b.id_category,  b.name, b.date, b.assurance, b.description, b.file,
             c.name AS comp_name, b.url FROM bill b, company c WHERE b.id_user = ' . $id_user . ' AND b.id_company = c.id AND b.deleted = 0 ';
        
        if (!is_null($id))
        {
             $query .= ' AND b.id IN (SELECT cat.id_bill FROM catalog cat WHERE cat.id_category ="' . $id . '" ) ';
        }
        
        if (!is_null($search))
        {
            $query .= ' AND (b.name LIKE "%' . addslashes($search) .'%" OR c.name LIKE "%' . addslashes($search) .'%" OR b.date LIKE "%' . addslashes($search) .'%") ';
        }
        
        if (!is_null($order))
        {            
            $columb = '';
            if ($order == ORDER_BY_NAME)
            {
                $query .= ' ORDER BY b.name';
                
                if ($order_by == ORDER_ASC)
                {
                    $query .= ' ASC';
                }
                if ($order_by == ORDER_DESC)
                {
                    $query .= ' DESC';
                }
            }
            if ($order == ORDER_BY_COMPANY)
            {
                $query .= ' ORDER BY c.name';
                
                if ($order_by == ORDER_ASC)
                {
                    $query .= ' ASC';
                }
                if ($order_by == ORDER_DESC)
                {
                    $query .= ' DESC';
                }
            }
            if ($order == ORDER_BY_DATE)
            {
                $query .= ' ORDER BY b.date';
                
                if ($order_by == ORDER_ASC)
                {
                    $query .= ' ASC';
                }
                if ($order_by == ORDER_DESC)
                {
                    $query .= ' DESC';
                }
            }
            
//            if ($order_by == ORDER_ASC)
//            {
//                $query .= ' ASC';
//            }
//            if ($order_by == ORDER_DESC)
//            {
//                $query .= ' DESC';
//            }
            
            //$query .= ' ORDER BY ' . $columb;
        }
        
        if (!is_null($start))
        {
            $query .= ' LIMIT ' . $start;
            if (!is_null($limit))
            {
                $query .= ', ' . $limit;
            }
        }
        
        $result = $database->query($query);
        
        if (!$result)
        {
            $errors->add( ERROR_SELECT,  'Selected bills from user');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Selected bills from user ' . $query);
            return NULL;
        }
        
        return $result;
    }
    
    /**
     * Select bills belong to user and have same company
     * @param myslqi    $database       database
     * @param uint      $id_user        id of user
     * @param uint      $id             url of category
     * @return null|string result
     */
    public function get_bills_simi( $database, $id_user, $id_comp, $id_bill)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$id_user || !$id_comp)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in get similar bills for user');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get similar bills for user');
            
            return NULL;
        }
        
        $query = 'SELECT b.id, b.id_user, b.id_company, b.id_category, b.name, cat.name AS cat_name, b.url
            FROM bill b, company c, category cat WHERE b.id_user = ' . $id_user . ' AND b.id_company = c.id AND b.id_category = cat.id
                AND b.deleted = 0 AND c.id = '. $id_comp . ' AND b.id != ' . $id_bill . ' LIMIT 6';
        
        $result = $database->query($query);
        
        if (!$result)
        {
            $errors->add( ERROR_SELECT,  'Selected similar bills from user');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Selected similar bills from user ' . $query);
            return NULL;
        }
        
        return $result;
    }
    
    /**
     * Select bill belong to user
     * @param myslqi    $database   database
     * @param uint      $url        url of bill
     * @return null|string result
     */
    public function get_bill_id( $database, $url)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$url)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in get bills for user');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get bills for user');
            return NULL;
        }        
        
        $query = 'SELECT b.id FROM bill b WHERE b.url = "' . addslashes($url) . 
             '" AND b.deleted = 0';
        
        $result = $database->query($query);
        
        if (!$result)
        {
            $errors->add( ERROR_SELECT,  'Select bill from user');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Select bill from user ' . $query);
            return NULL;
        }
        
        return $result;
    }
    
    /**
     * Select bill belong to user
     * @param myslqi    $database   database
     * @param uint      $id         id of bill
     * @return null|string result
     */
    public function get_bill( $database, $url)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$url)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in get bills for user');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get bills for user');
            return NULL;
        }        
        
        $query = 'SELECT b.id, b.id_user, b.id_company, b.id_category, b.name, b.date, b.assurance, b.description, b.file,
             c.name AS comp_name, cat.name AS cat_name FROM bill b, company c, category cat WHERE b.url = "' . addslashes($url) . 
             '" AND b.id_company = c.id AND b.id_category = cat.id AND b.deleted = 0';
        
        $result = $database->query($query);
        
        if (!$result)
        {
            $errors->add( ERROR_SELECT,  'Select bill from user');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Select bill from user ' . $query);
            return NULL;
        }
        
        return $result;
    }
    
    /**
     * Delete bill belong to user
     * @param myslqi    $database   database
     * @param uint      $id         id of bill
     * @return boolean  succes of delete
     */
    public function delete( $database, $id)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$id)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in delete bill');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in delete bill');
            return FALSE;
        }
        
        //$query = 'DELETE FROM bill b WHERE b.id = ' . $id;
        $query = 'UPDATE bill SET deleted= 1 WHERE id=' . $id;        
        
        $result = $database->query($query);
        
        if (!$result)
        {
            $errors->add( ERROR_SELECT,  'Delete bills from user');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Delete bills from user');
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function get_rows_count($database, $id_user, $id, $search = null)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$id_user)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in get row count');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get row count');
            return 0;
        }
        
        $query = 'SELECT b.id FROM bill b, company c WHERE b.id_company = c.id AND b.deleted = 0 AND b.id_user = ' . $id_user;        
        
        if (!is_null($id))
        {
             $query .= ' AND b.id IN (SELECT cat.id_bill FROM catalog cat WHERE cat.id_category ="' . $id . '" ) ';
        }
        
        if (!is_null($search))
        {
            $query .= ' AND (b.name LIKE "%' . $search .'%" OR c.name LIKE "%' . $search .'%" OR b.date LIKE "%' . $search .'%") ';
        }
        
        $result = $database->query($query);
        
        if (!$result)
        {
            $errors->add( ERROR_SELECT,  'Get row count bills from user');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Get row count bills from user');
            return 0;
        }
        
        $count = $database->get_num_rows($result);
        
        return $count;
    }
}

?>
