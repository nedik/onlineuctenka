<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Company
 *
 * @author redak105
 */
class DB_Company 
{
    private static $instance = NULL;

    /**
     * Constructor
     */
    function __construct() {
        ;
    }
    
    /**
     * Destructor
     */
    function __destruct() {
        ;
    }
    
    /**
     * Return instance of class
     * @return instance of class DB_Company
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) {
            self::$instance = new $class;
        }
        return self::$instance;
    }
    
    /**
     * Inser new company
     * @param mysqli    $database   database
     * @param uint      $id_user    id of company
     * @param string    $name       name of company
     * @return boolean result
     */
    public function insert( $database, $id_user, $name)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$id_user || !$name)
        {
            $errors->add( ERROR_INPUT, 'ERROR input parameters in insert company');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get parrent category');
            return FALSE;
        }
        
        $query = 'SELECT c.id, c.id_user, c.name FROM company c WHERE c.name = "' . addslashes($name) . '" AND c.id_user = ' . $id_user;
        
        $result = $database->query($query);
        $row = mysqli_fetch_assoc($result);
        if ($row)
        {
            $errors->add( ERROR_INSERT, 'Company name alreadu exist');
            $logger->add( time(), ERROR_INSERT, debug_backtrace(), 'Company name alreadu exists ');
            return FALSE;
        }
        
        $query = 'INSERT INTO company( id, id_user, name) VALUES ( NULL, ' . $id_user . ', "'  . addslashes($name) . '");';
        if (!$database->query($query))
        {
            $errors->add( ERROR_INSERT, 'Insert new company');
            $logger->add( time(), ERROR_INSERT, debug_backtrace(), 'Insert new company ' . $query);
            return FALSE;
        }
        return TRUE;
    }
    
    /**
     * Select company by id of user or name
     * @param mysqli    $database   database
     * @param uint      $id_user    id of company
     * @param string    $name       name of company
     * @return null|string  result
     */
    public function select( $database, $id_user, $name)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in select company');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in select company');
            return NULL;
        }
        
        $query = 'SELECT c.id, c.id_user, c.name FROM company c WHERE c.deleted = 0' ;
        
        if ($id_user)
        {
            $query .= ' AND c.id = ' . $id_user;
        }
        
        if ($name)
        {
            $query .= 'AND c.name = ' . addslashes($name);
        }
        
        
        $result = $database->query($query);
        if (!$result)
        {
            $errors->add( ERROR_SELECT, 'Company selection');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Company selection ' . $query);
            return NULL;
        }
        
        return $result;
    } 
    
    /**
     * Delete company belong to user
     * @param myslqi    $database   database
     * @param unit      $user       id of user
     * @param uint      $id         id of bill
     * @return boolean  succes of delete
     */
    public function delete( $database, $id_user, $id)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        $notifi = Notification::get_instance();
        
        if (!$database || !$id_user || !$id)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in delete company');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in delete company');
            return FALSE;
        }
        
        
        $query = 'SELECT b.id FROM bill b WHERE b.deleted = 0 AND b.id_company = ' . $id . ' AND b.id_user = ' . $id_user;
        
        $result = $database->query($query);
        $row = mysqli_fetch_assoc($result);
        if (!$row)
        {
            $query = 'SELECT b.id FROM bill b WHERE b.deleted = 1 AND b.id_company = ' . $id . ' AND b.id_user = ' . $id_user;
            $result = $database->query($query);
            $row = mysqli_fetch_assoc($result);
            if ($row)
            {                
                $query = 'UPDATE company SET deleted = 1 WHERE id=' . $id;  
            }
            else {
                $query = 'DELETE FROM company WHERE id = ' . $id . ' AND id_user = ' . $id_user;
            }
        }
        else {
            $errors->add( ERROR_SELECT,  'Bill with this company exist');
            $notifi->add( NOTIFI_NORMAL, 'Existuje účtenka s touto firmou');            
            return FALSE;
        }     
        
        $result = $database->query($query);
        
        if (!$result)
        {
            $errors->add( ERROR_SELECT,  'Delete company from user');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Delete company from user ' .$query);
            return FALSE;
        }
        
        return TRUE;
    }
    
    /**
     * Select companies for user
     * @param mysqli    $database   database
     * @param uint      $id_user    id of user
     * @return null|string  result
     */
    public function get_companyies( $database, $id_user, $start = NULL, $limit = NULL)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if ( !$database || !$id_user)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in get companies for user');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get companies for user');
            return NULL;
        }
        
        $query = 'SELECT c.id, c.id_user, c.name FROM company c WHERE c.deleted = 0 AND c.id_user = ' . $id_user;
                
        if (!is_null($start))
        {
            $query .= ' LIMIT ' . $start;
            if (!is_null($limit))
            {
                $query .= ', ' . $limit;
            }
        }
        
        $result = $database->query($query);
        
        if (!$result)
        {
            $errors->add( ERROR_SELECT, 'Selection companies from user');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Selection companies from user');
            return NULL;
        }
        
        return $result;
    }
    
    public function get_rows_count($database, $id_user)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database || !$id_user)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in get row count');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get row count');
            return 0;
        }
        
        $query = 'SELECT c.id FROM company c WHERE c.deleted = 0 AND c.id_user = ' . $id_user;                        
        
        $result = $database->query($query);
        
        if (!$result)
        {
            $errors->add( ERROR_SELECT,  'Get row count comapnyies from user');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Get row count comapnyies from user');
            return 0;
        }
        
        $count = $database->get_num_rows($result);
        
        return $count;
    }
}

?>
