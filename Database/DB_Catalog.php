<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'Errors.php';

/**
 * Description of Catalog
 *
 * @author redak105
 */
class DB_Catalog
{
    private static $instance = NULL;

    /**
     * Constructor
     */
    function __construct() {
        ;
    }
    
    /**
     * Destructor
     */
    function __destruct() {
        ;
    }
    
    /**
     * Return instance of class
     * @return instance of class DB_Catalog
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) {
            self::$instance = new $class;
        }
        return self::$instance;
    }
    
    /**
     * Insert new catalog
     * @param mysqli    $database       database
     * @param uint      $id_user        id of user
     * @param uint      $id_category    id of category
     * @param uint      $id_bill        id of bill
     * @return boolean succes of inser
     */
    public function insert( $database, $id_user, $id_category, $id_bill)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();  
        
        if (!$database || !$id_user || !$id_category || !$id_bill)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in insert category');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in insert category');
            
            return FALSE;
        }
        
        $query = 'SELECT c.id FROM catalog c WHERE c.id_bill = ' . $id_bill;
        $result = $database->query($query);
        $row = mysqli_fetch_assoc($result);
        if ($row)
        {                
            $errors->add( ERROR_REGISTR_EXIST, 'Category id exists in insert category');
            return FALSE;
        }
        
        //search category id
        $query = 'SELECT c.category_id, c.parent_id FROM category c WHERE c.id = ' . $id_category;
        $result = $database->query($query);
        $row = mysqli_fetch_assoc($result);
        if (!$row)
        {                
            $errors->add( ERROR_REGISTR_EXIST, 'Category id not exist in insert category');
            return FALSE;
        }
        
        //insert categories in tree
        $this->insert_parent( $database, $id_user, $row['category_id'], $id_bill);
        
        return TRUE;
    }
    
    /**
     * Insert parrent category to catalog
     * @param mysqli    $database       database
     * @param uint      $id_user        id of user
     * @param uint      $category_id    category id
     * @param uint      $id_bill        id of bill
     * @return boolean succes of insert
     */
    private function insert_parent( $database, $id_user, $category_id, $id_bill)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();  
        
        $query = 'SELECT c.id, c.parent_id FROM category c WHERE c.category_id = ' . $category_id;
        $result = $database->query($query);
        $row = mysqli_fetch_assoc($result);
        if (!$row)
        {                
            $errors->add( ERROR_REGISTR_EXIST, 'Category id not exist in insert category');
            $logger->add( time(), ERROR_REGISTR_EXIST, debug_backtrace(), 'Category id not exist in insert category');
            return FALSE;
        }
        
        $query = 'INSERT INTO catalog( id, id_user, id_category, id_bill) VALUES ( NULL, ' . $id_user . ', ' . $row['id'] . ', ' . $id_bill . ');';
       
        $result = $database->query($query);
        if (!$result)
        {
            $errors->add( ERROR_INSERT, 'Insert new catalog');
            $logger->add( time(), ERROR_INSERT, debug_backtrace(), 'Insert new catalog');
            
            return FALSE;
        }
        
        if (!$row['parent_id'] == 0)
        {
            $this->insert_parent( $database, $id_user, $row['parent_id'], $id_bill);
        }
        
        return TRUE;   
    }
    
    /**
     * Update category
     * @param mysqli    $database       database
     * @param uint      $id_user        id of user
     * @param uint      $id_category    id of category
     * @param uint      $id_bill        id of bill
     * @return boolean succes of delete
     */
    public function update( $database, $id_user, $id_category, $id_bill)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();  
        
        if (!$database || !$id_user || !$id_category || !$id_bill)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in insert category');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in insert category');
            return FALSE;
        }
        
        $query = 'DELETE FROM catalog WHERE id_bill =' . $id_bill . '';
        $result = $database->query($query);
        if (!$result)
        {                
            $errors->add( ERROR_DELETE, 'Delete existing category');
            $logger->add( time(), ERROR_DELETE, debug_backtrace(), 'Delete existing category ' . $query);
            return FALSE;
        }
       
        $query = 'SELECT c.category_id, c.parent_id FROM category c WHERE c.id = ' . $id_category;
        $result = $database->query($query);
        $row = mysqli_fetch_assoc($result);
        if (!$row)
        {                
            $errors->add( ERROR_REGISTR_EXIST, 'Category id not exist in insert category');
            return FALSE;
        }
        
        $this->insert_parent( $database, $id_user, $row['category_id'], $id_bill);
        
        return TRUE;
    }
    
    /**
     * Delete category
     * @param mysqli    $database       database
     * @param uint      $id_bill        id of bill
     * @return boolean succes of delete
     */
    public function delete( $database, $id_bill)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance(); 
        
        if (!$database || !$id_bill)
        {
            $errors->add( ERROR_INPUT, 'Input parameters in insert category');
            return FALSE;
        }
        
        $query = 'DELETE FROM catalog WHERE id_bill = ' . $id_bill;
        $result = $database->query($query);
        if (!$result)
        {                
            $errors->add( ERROR_DELETE, 'Delete existing category');
            return FALSE;
        }
        
        return TRUE;
    }
    
    /**
     * Select categories
     * @param mysqli    $database   database
     * @return null|string result
     */
    public function get_catalogs( $database, $user_id)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database)
        {
            $errors->add( ERROR_INPUT,'Input parameters in get catalogs');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get catalogs');
            return NULL;
        }
        
        $query = 'SELECT c.id, c.id_user, c.id_category, c.id_bill FROM catalog c WHERE c.id_user = ' . $user_id;
                
        $result = $database->query($query);
        if (!$result)
        {
            $errors->add( ERROR_SELECT, 'Categories selection');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Categories selection ' . $query);
            return NULL;
        }
        
        return $result;
    }
    
    /**
     * Select categoryies from catalog
     * @param mysqli    $database   database
     * @param uint      $id_user    id of user
     * @param uint      $parent_id  parent id
     * @param string    $search     search string
     * @return null|string result
     */
    public function get_categoryies( $database, $id_user, $parent_id, $search = NULL)
    {
        $errors = Errors::get_instance();
        $logger = Logger::get_instance();
        
        if (!$database)
        {
            $errors->add( ERROR_INPUT,'Input parameters in get catalogs');
            $logger->add( time(), ERROR_INPUT, debug_backtrace(), 'Input parameters in get catalogs');
            return $result;
        }
        
//        $query = 'SELECT cat.id, cat.category_id, cat.name, cat.url, COUNT(cat.category_id) AS count FROM catalog c, category cat
//            WHERE cat.id = c.id_category AND c.id_user = ' . $id_user . ' AND cat.parent_id = ' . $parent_id . ' GROUP BY cat.category_id';
        
        $query = 'SELECT cat.id, cat.category_id, cat.name, cat.url, COUNT(cat.category_id) AS count FROM catalog c, category cat
            WHERE cat.id = c.id_category AND c.id_user = ' . $id_user . ' AND cat.parent_id = ' . $parent_id;
        
        if(!is_null($search))
        {        
            $query .= ' AND c.id_bill IN (SELECT b.id FROM bill b, company c 
                WHERE b.id_company = c.id AND b.deleted = 0 AND (b.name LIKE "%' . $search .'%" OR c.name LIKE "%' . $search .'%" OR b.date LIKE "%' . $search .'%"))';
        }
                        
        $query .= ' GROUP BY cat.category_id';
        
        $result = $database->query($query);
        if (!$result)
        {
            $errors->add( ERROR_SELECT, 'Categories selection');
            $logger->add( time(), ERROR_SELECT, debug_backtrace(), 'Categories selection ' . $query);
            return $result;
        }
        
        return $result;
    }
}
    
?>