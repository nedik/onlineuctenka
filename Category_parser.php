<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('Database/Database.php');
require_once('Database/DB_Category.php');

/**
 * Update list of categories
 * @param Database  $database   class of Database
 * @param string    $url        url to get xml file
 */
function update_categoryies( $database, $url)
{
    echo "Opening XML from web" . '<br>';
    $filename = "example.xml";
         
    $ch = curl_init( $url);
    $fp = fopen( $filename, "w");

    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);

    curl_exec($ch);
    curl_close($ch);
    fclose($fp);
            
    echo "Close XML file" . '<br>';
    
    echo "Insert XML to table" . '<br>';
            
    $cat = new SimpleXMLElement( $filename, null, true);
        
    $db_category = new DB_Category();
    
    subcategory_parser( $database, $db_category, $cat);
    
    echo "Insert XML to table finish" . '<br>';
}

/**
 * Update subcategoryies of category
 * @param Database $database        class of Database
 * @param Category $db_category     class of DB_Category
 * @param string   $category        selected category
 */
function subcategory_parser( $database, $db_category, $category)
{
    foreach ($category->CATEGORY as $subcategory) 
    {
        echo $subcategory->CATEGORY_ID, ' - ', $subcategory->CATEGORY_NAME, "<br>";
        
        $url = $subcategory->CATEGORY_NAME;
        $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
        $url = trim($url, "-");
        echo $url . '<br>';
        setlocale(LC_CTYPE, 'cs_CZ'); 
        $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);  
//        $url = StrTr($url, "ÁÄČÇĎÉĚËÍŇÓÖŘŠŤÚŮÜÝŽáäčçďéěëíňóöřšťúůüýž", "AACCDEEEINOORSTUUUYZaaccdeeeinoorstuuuyz");
        $url = strtolower($url);
        $url = preg_replace('~[^-a-z0-9_]+~', '', $url);
        
        echo $url . '<br>';
        
        $db_category->insert( $database, $category->CATEGORY_ID, $subcategory->CATEGORY_ID, $subcategory->CATEGORY_NAME, $url);
        
        if (@count($subcategory->children()) != 0)
        {
            subcategory_parser( $database, $db_category, $subcategory);
        }   
    }
}


?>
