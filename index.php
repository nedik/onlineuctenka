<?php
    error_reporting(E_ALL);
    
    session_start();
    
//   ini_set('memory_limit', '-1');
    
    require_once 'Classes/User.php';
    require_once 'Classes/Bill.php';
    require_once 'Database/DB_Category.php';
    require_once 'Errors.php';
    require_once 'Metadata.php';
    require_once 'router.php';
    require_once 'parser.php'; 
    
    $user = User::init();
    
    $parser = Parser::get_instance();
    $parser->init();
    
    $router = Router::get_instance();
    
    $page_info =  $router->init($user, $parser);
    
    $notifi = Notification::get_instance();
    $notifi->init();
    
    
//    require_once('Category_parser.php');
//    $db = Database::get_instance();
//    $db->connect();
//    update_categoryies($db, "http://www.heureka.cz/direct/xml-export/shops/heureka-sekce.xml");
//    $db->disconnect();
    
//    echo 'id ' . $_SESSION['id_user'] . ' email' . $_SESSION['email'] . '<br>';
            
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="description" content="<?php echo $page_info['meta']['description'] ?>">
        <meta name="text" content="<?php echo $page_info['meta']['text'] ?>">
        <meta name="keywords" content="<?php echo $page_info['meta']['keywords'] ?>">

        <link rel="stylesheet" type="text/css" href="<?php echo HTTP ?>css/header.css">
        <link rel="stylesheet" type="text/css" href="<?php echo HTTP ?>css/footer.css">
        <link rel="stylesheet" type="text/css" href="<?php echo HTTP . $page_info['css'] ?>">
        
        <script src="<?php echo HTTP ?>jquery/js/jquery-1.8.3.js"></script>
        <script src="<?php echo HTTP ?>jquery/js/jquery-ui-1.9.2.custom.js"></script>
        <!--<script src="<?php echo HTTP ?>jquery/js/jquery.ui.dialog.js"></script>-->
        
        <link rel="stylesheet" type="text/css" href="<?php echo HTTP ?>jquery/css/custom-theme/jquery-ui-1.9.2.custom.css">         
        
        <title><?php echo $page_info['meta']['title'] ?></title>
    </head>
    
    <body>
        <?php require_once $page_info['header']; ?>
        <?php require_once $page_info['page']; ?>
        
        <?php require_once $page_info['footer']; ?>
    </body>
</html>


<?php

//    $errors = Errors::get_instance();
    
//    $errors->print_errors();
?>