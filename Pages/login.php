<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    if (!isset($_SESSION['login_username']))
    {
        $login_username = 'abc@abc.com';
        $_SESSION['login_username'] = $login_username;
    }
    if (!isset($_SESSION['login_password']))
    {
        $login_password = 'abc@abc.com';
        $_SESSION['login_password'] = $login_password;
    }
    
    $notifi = Notification::get_instance();
    
?>

<div id="login">
    <div id="login_1">
        <div id="login_1_in">            
            <h1>Přihlášení</h1>
        </div>
    </div>
    
    <div id="login_2">
        <div id="login_2_in">
            <?php $notifi->print_notifi() ?>
            <div class="clearer" ></div>
            
            <form class="login"  action="" method="post" accept-charset="UTF-8"> 
                <div>
                    <fieldset>
                        <div class="clear">
                            <label for="f_username"> E-mail </label>
                            <input id="f_username" type="text" name="email" maxlength="40" value="<?php echo $_SESSION['login_username'] ?>"> 
                        </div>     
                        <div class="clearer" ></div>
                        <div class="clear">
                            <label for="f_password"> Heslo </label>
                            <input id="f_password" type="password" name="password" maxlength="50" value="<?php echo $_SESSION['login_password'] ?>"> 
                        </div>
                        <div class="clearer" ></div>
                        <div>  
                            <input id="submit" type="image" name="submit" value="<?php echo SUBMIT_LOGIN ?>" src="img/login/login.png">
                            <input type="hidden" name="submit" value="<?php echo SUBMIT_LOGIN ?>">
                        </div>
                         
                        <p > 
                            <a href="<?php echo HTTP . ACTION_REG ?>">Registrace</a>  
                        </p>
                    </fieldset>
                </div>   
            </form> 
            <div class="clearer" ></div>
        </div>
    </div>
</div>