<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    $parser = Parser::get_instance();
    $url = $parser->get_url();
    $order = $parser->get_order();
    $by = $parser->get_order_by();

?>

<script>
$(document).ready(function(){
$('#datepicker').datepicker({
inline: true
});
});
</script>   



<div id="main">        
    <div id="main_in">
        <?php $notifi->print_notifi() ?>
        <div class="clearer" ></div>
        
        <div class="left">
            <a  href="<?php echo HTTP . ACTION_BILL_CREATE ?>"><img src="<?php echo HTTP ?>img/main/create.png" alt="Vytvořit účtenku"></a>            
            <div id="category_list">
                <?php require_once PAGE_CATEGORY_LIST; ?>
            </div>            
            <div class="clearer" ></div>
            <!--<div id="datepicker"></div>-->
        </div>
        
        <div class="right">
            <div id="menu">
                <?php 
                
                    $a = '<a href="' . HTTP;
                                       
                    $search = $parser->get_search();
                    if (is_null($search))
                    {
                        $a .= ACTION_LIST . $url . '?' . ORDER . '=';
                    }
                    else {
                        $a .= ACTION_SEARCH . $url . '?q=' . $search . '&' . ORDER . '=';
                    }
                    
                    $a_name = $a . ORDER_BY_NAME . '&' . ORDER_BY . '=';
                    if ($order == ORDER_BY_NAME)
                    {
                        echo '<div class="item_s">';
                        echo 'Název ';
                        
                        if ($by == ORDER_ASC)
                        {
                            echo $a_name . ORDER_ASC . '"><img src="' . HTTP. 'img/main/order_bot_1.png" alt=""></a>';
                            echo $a_name . ORDER_DESC . '"><img src="' . HTTP . 'img/main/order_top.png" alt=""></a>';
                        }
                        if ($by == ORDER_DESC)
                        {
                            echo $a_name . ORDER_ASC . '"><img src="' . HTTP. 'img/main/order_bot.png" alt=""></a>';
                            echo $a_name . ORDER_DESC . '"><img src="' . HTTP . 'img/main/order_top_1.png" alt=""></a>';
                        }
                    }
                    else {
                        echo '<div class="item">';
                        echo 'Název ';
                        echo $a_name . ORDER_ASC  . '"><img src="' . HTTP. 'img/main/order_bot.png" alt=""></a>';
                        echo $a_name . ORDER_DESC . '"><img src="' . HTTP . 'img/main/order_top.png" alt=""></a>';
                    }                    

                    echo '</div>';
   
                    $a_company = $a . ORDER_BY_COMPANY . '&' . ORDER_BY . '=';
                    if ($order == ORDER_BY_COMPANY)
                    {
                        echo '<div class="item_s">';
                        echo 'Firma ';
                        if ($by == ORDER_ASC)
                        {
                            echo $a_company . ORDER_ASC . '"><img src="' . HTTP. 'img/main/order_bot_1.png" alt=""></a>';
                            echo $a_company . ORDER_DESC . '"><img src="' . HTTP . 'img/main/order_top.png" alt=""></a>';
                        }
                        if ($by == ORDER_DESC)
                        {
                            echo $a_company . ORDER_ASC . '"><img src="' . HTTP. 'img/main/order_bot.png" alt=""></a>';
                            echo $a_company . ORDER_DESC . '"><img src="' . HTTP . 'img/main/order_top_1.png" alt=""></a>';
                        }
                    }
                    else {
                        echo '<div class="item">';
                        echo 'Firma ';
                        echo $a_company . ORDER_ASC . '"><img src="' . HTTP. 'img/main/order_bot.png" alt=""></a>';
                        echo $a_company . ORDER_DESC . '"><img src="' . HTTP . 'img/main/order_top.png" alt=""></a>';
                    }     

                    echo '</div>';
               
                    $a_date= $a . ORDER_BY_DATE . '&' . ORDER_BY . '=';
                    if ($order == ORDER_BY_DATE)
                    {
                        echo '<div class="item_s">';
                        echo 'Datum ';
                        if ($by == ORDER_ASC)
                        {
                            echo $a_date . ORDER_ASC . '"><img src="' . HTTP. 'img/main/order_bot_1.png" alt=""></a>';
                            echo $a_date . ORDER_DESC . '"><img src="' . HTTP . 'img/main/order_top.png" alt=""></a>';
                        }
                        if ($by == ORDER_DESC)
                        {
                            echo $a_date . ORDER_ASC . '"><img src="' . HTTP. 'img/main/order_bot.png" alt=""></a>';
                            echo $a_date . ORDER_DESC . '"><img src="' . HTTP . 'img/main/order_top_1.png" alt=""></a>';
                        }
                    }
                    else {
                        echo '<div class="item">';
                        echo 'Datum ';
                        echo $a_date . ORDER_ASC . '"><img src="' . HTTP. 'img/main/order_bot.png" alt=""></a>';
                        echo $a_date . ORDER_DESC . '"><img src="' . HTTP . 'img/main/order_top.png" alt=""></a>';
                    }     

                    echo '</div>';
                ?>
            </div>
            
            <div>
                <?php require_once PAGE_BILLS; ?>
            </div>
        </div>
        <div class="clearer" ></div>        
    </div>
    
    <div class="clearer" ></div>
</div>
