<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    require_once 'Notification.php';
    require_once 'Classes/User.php';
    require_once 'Database/DB_Company.php';
    require_once 'Database/Database.php';

    $user = User::init();
    
    if (!$user->is_logged())
    {
       header("Location: " . HTTP . ACTION_FRONT);
       exit;
    }
    
    $parser = Parser::get_instance();
    
    $notifi = Notification::get_instance();
        
?>


<div id="company">   
    <div id="company_in">
        <?php $notifi->print_notifi() ?>
        <div class="clearer" ></div>
        
        <form class="add"  action="" method="post" accept-charset="UTF-8"> 
                    <div class="insert">
                        <label> Nová firma </label>
                        <input  class="text" type="text" name="name" maxlength="ž0" value=""> 
                        <!--<input id="submit" type="image" name="submit" value="<?php echo SUBMIT_COMPANY ?>" src="img/login/login.png">-->
                        <input id="submit" type="image" name="submit" value="<?php echo SUBMIT_COMPANY ?>"  src="<?php echo HTTP ?>img/create_company.png" alt="Přidat firmu">
                        <input type="hidden" name="submit" value="<?php echo SUBMIT_COMPANY ?>">
                    </div>
            <div class="clearer" ></div>
        </form>
        
        <div class="companyies">
            <?php get_companyies($parser, $user) ?>            
        </div>    
                
        <div id="back">
            <a  href="<?php echo HTTP . ACTION_LIST; ?>"><img src="img/main/arrow.png" alt="Zpět">Zpět</a>
        </div>
        
        <div class="clearer" ></div>        
    </div>
</div>

<?php
    /**
     * Create list of companies belong to user
     * @param Parser    $parser  Parser class
     * @param User      $user    User class
     * @return 
     */
    function get_companyies($parser, $user)
    {
        $db = Database::get_instance();
        $db->connect();

        $company = DB_Company::get_instance();
    
        $max = $company->get_rows_count( $db, $user->__get('id'));
            
        $page = $parser->get_page();
        
        $max_page = ceil($max / ON_PAGE);
        
        if ($max_page <= 0)
        {
            $max_page = 1;
        }
        
        if ($page > $max_page)
        {
            $page = $max_page;
        }
        
        $start = ($page-1) * ON_PAGE;
    
        $data = $company->get_companyies( $db, $user->__get('id'), $start, ON_PAGE);
        
        $db->disconnect();  
        
        if (is_null($data))
        {
            return;
        }
        
        while ($row = $db->fetch_array($data)) 
        {
            echo '<div class="company">';
            echo '<div class="data">' . $row['name'] . '</div>';
            echo '<a class="delete" href="?submit=' . SUBMIT_COMPANY_DELETE . '&id=' . $row['id'] . '"> smazat </a>';
            echo '<div class="clearer" ></div>';
            echo '</div> ';
        }
        
        $db->disconnect();
        
        echo '<div class="listing">';
    
        $url_s = HTTP . $parser->get_action() .  $parser->get_url() . '?' . ACTUAL_PAGE . '=';
        $url_e = '';

        if($page > 1) 
        {
            echo '<a href="' . $url_s . '1' . $url_e . '">&lt;&lt;</a>';
        }

        if($page > 1) 
        { 
              echo '<a href="' . $url_s . ($page - 1) . $url_e . '">&lt;</a>';

              for($i = 4; $i > 0; $i--) 
              {
                    if(($page - $i) >= 1){
                        echo '<a href="' . $url_s . ($page - $i) . $url_e . '">' . ($page - $i) . '</a>';
                    }
              }
        }

        if ($max_page > 1)
        {
            echo $page;
        }

        if($page < $max_page) 
        {
            for($i = 1; $i < 4; $i++) 
            { 
                if(($page + $i) <= $max_page) 
                {
                    echo '<a href="' . $url_s . ($page + $i) . $url_e . '">' . ($page + $i) . '</a>';
                }
            }
            echo '<a href="' . $url_s . ($page + 1) . $url_e . '">&gt;</a>';
        }

        if($page < $max_page) 
        {
            echo '<a href="' . $url_s . ceil($max / ON_PAGE) . $url_e  . '">&gt;&gt;</a>';
        }

        echo '</div>';

        return;
    }  
?>