<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    require_once('Classes/User.php');
    require_once('Database/Database.php');
    require_once('Database/DB_Bill.php');
    
    $user = User::init();
    
    if (!$user->is_logged())
    {
        header("Location: " . HTTP . ACTION_LOGIN);
        exit;
    }            
    
    $parser = Parser::get_instance();            
    
    $id = 0;
    $array = $parser->get_array();
    
    if ($array)
    {
        $id = end($array);
    }      
    else {
        require_once PAGE_ERROR;
        exit;
    }
    
    
    $db = Database::get_instance();
    $db->connect();
        
    $bills = DB_Bill::get_instance();   
    $bill = $bills->get_bill( $db, $id);
    
    $bill = $db->fetch_array($bill);
    
    $data = $bills->get_bills_simi( $db, $user->__get('id'), $bill['id_company'], $bill['id']);    
       
    while ($row = $db->fetch_array($data)) 
    {
        echo '<a href="' . HTTP . ACTION_BILL_DETAIL . '/' . $row['url'] . '">';
        echo '<div class="bill">';
        echo $row['name'];
        echo '</br>';
        echo $row['cat_name'];
        echo '</div>';        
        echo '</a>';
    }
    
    $db->disconnect();
    
    return;

?>

