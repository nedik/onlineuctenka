<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    require_once 'Classes/User.php';
    require_once 'Classes/Bill.php';
    require_once 'Classes/File_manager.php';
    require_once 'Database/Database.php';
    require_once 'Database/DB_Bill.php';
    require_once 'Settings/conf.php';   
    
    $user = User::init();
    
    if (!$user->is_logged())
    {
        header("Location: " . HTTP . ACTION_FRONT);
        exit;
    }
    
    $notifi = Notification::get_instance();
    
    $parser = Parser::get_instance();
    
    
    $id = 0;
    $array = $parser->get_array();
    
    if ($array)
    {
        $id = end($array);
    }      
    else {
        require_once PAGE_ERROR;
        exit;
    }
    
    $bill = Bill::get_instance();
    $data = $bill->get_bill( $user, $id);
    
    if (is_null($data))
    {
        require_once PAGE_ERROR;
        exit;
    }
    
    $file = File_manager::get_instance();
    
    $path = HTTP . conf::$directory . '/' . $user->__get('id') . '/' . $data['id'] . '.' . $file->get_extension($data['file']);        
?>

<div id="detail">        
    <div id="detail_in">
        <?php $notifi->print_notifi() ?>
        <div class="clearer" ></div>
        
        <div id="zoom">
        </div>
        
        <div class="left">
            <div>
                <div class="name">
                    Název:
                </div>
                <div class="data">
                    <?php echo $data['name'];?>
                </div>
            </div>
            <div>
                <div class="name">
                    Firma:
                </div>
                <div class="data">
                    <?php echo $data['comp_name'];?>
                </div>
            </div>
            <div>
                <div class="name">
                    Kategorie:
                </div>
                <div class="data">
                    <?php echo $data['cat_name'];?>
                </div>
            </div>
            <div>
                <div class="name">
                    Datum pořízení:
                </div>
                <div class="data">
                    <?php echo date( 'd. n. Y', strtotime($data['date']));  ?>
                </div>
            </div>
            <div>
                <div class="name">
                    Záruka:
                </div>
                <div class="data">
                    <?php echo $data['assurance'];?> mesicu
                </div>
            </div>
            <div>
                <div class="name">
                    Popis:
                </div>
                <div class="data">
                    <?php echo $data['description'];?>
                </div>
            </div>
            <div class="clearer" ></div> 

            <div id="image"> 
                <!--<a id="image_a" href="<?php //echo $path ?>" title="Kliknutím zvětšíte">-->
                    <img id="image_bill" src="<?php echo $path . '?' . time(); ?>" width="200" alt="Obrázek účetnky">  
                <!--</a>-->                                
            </div>
            
            <div id="print">
                <input type="button" onclick="printImg()" value="PRINT">
            </div>
            
            <div id="back">
                <a  href="<?php echo $_SERVER['HTTP_REFERER']; ?>"><img src="<?php echo HTTP ?>img/main/arrow.png" alt="Zpět">Zpět </a>
            </div>
            
        </div>
        
        <div class="right">           
            
            <a  href="<?php echo HTTP . ACTION_BILL_EDIT . '/' . $id ?>"><img src="<?php echo HTTP ?>img/main/edit.png" alt="Upravit účtenku" ></a>
                <div class="clearer" ></div>
                <a  href="<?php echo HTTP .'?submit=' . SUBMIT_DELETE . '&id=' . $id ?>"><img src="<?php echo HTTP ?>img/main/delete.png" alt="Smazat účtenku" ></a>
                <div class="clearer" ></div>
                        
            <div id="similar">
                <h3>Podobné účtenky</h3>
                <div>
                    <?php require_once PAGE_BILL_SIMILAR; ?>                                        
                </div>
                
            </div>
        </div>
        <div class="clearer" ></div>      
    </div>

</div>


<script language="JavaScript" type="text/javascript">    
  
var bill = document.createElement("img");
var print = document.createElement("input");

var zoom = document.getElementById("zoom");      

zoom.appendChild(bill); 
zoom.appendChild(print); 
  
bill.style.display = "none";
print.style.display = "none";

var preview = document.getElementById("image_a");
  
preview.onclick = function()
{
    bill.src = this.href;    
    bill.id = "image_bill";
    bill.width = "500";
    
    bill.style.display = "block";   
    bill.style.position = "absolute";
    bill.style.backgroundColor = "#ffffaa";
    bill.style.border = "2px solid green";
    
    print.style.display = "inherit";
    print.style.position = "absolute";
    //print.href = "javascript:window.print()";
    //print.href = "<?php echo $path ?>";
    //print.href = "JavaScript:window.print();"
    print.type = "button"; 
    print.value ="Print Image";
    print.onclick = "printImg()";
    print.text = "Tisk";
    
    
    return false;
}  

bill.onclick = function()   
{
    bill.style.display = "none";
    //bill.style.padding = 0;
    //bill.style.border = 0;
    
    print.style.display = "none";
} 
</script>


<script type="text/javascript">
    
function printImg() {
  pwin = window.open(document.getElementById("image_bill").src,"_blank");
  pwin.onload = function () {window.print();}
    
    setTimeout("pwin.print()", 500);
    
//      var URL = "<?php //echo $path ?>";
//
//      var W = window.open(URL);
//
//      W.onload = "window.print()";
//      W.window.print();

}
</script>