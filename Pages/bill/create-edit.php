<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    require_once 'Classes/User.php';
    require_once 'Database/DB_Bill.php';
    require_once 'Database/DB_Category.php';
    require_once 'Database/Database.php';

    $user = User::init();
    
    if (!$user->is_logged())
    {
       header("Location: " . HTTP . ACTION_FRONT);
       exit;
    }
    
    $notifi = Notification::get_instance();
    
    $url = null;
    
    $parser = Parser::get_instance();
    $array = $parser->get_array();
    
    if ($array)
    {
        $url = end($array);
    }   
    
    $id = 0;
    $name = '';
    $company_id = 0;
    $company_new = '';
    $category_id = 0;
    $date = '';
    $assurance = 24;
    $description = '';
    $file = '';
    $path = '';     
    
    //search bill
    if (!is_null($url))
    {   
        $bill = Bill::get_instance();
        $data = $bill->get_bill( $user, $url);
        
        if (!is_null($data))
        {
            $id = $data['id'];
            $name = $data['name'];
            $company_id = $data['id_company'];
            $category_id = $data['id_category'];
            $date = date( 'd.n.Y', strtotime($data['date']));
            $assurance = $data['assurance'];
            $description = $data['description'];
            $file = $data['file'];
            
            $files = File_manager::get_instance();
            $path = HTTP . conf::$directory . '/' . $user->__get('id') . '/' . $data['id'] . '.' . $files->get_extension($data['file']);
        }
        else {
            require_once PAGE_ERROR;
            exit;
        }
    }
    
    else {
        if (isset($_SESSION['bill_name']))
        {
            $name = $_SESSION['bill_name'];
        }

        if (isset($_SESSION['bill_company_id']))
        {
            $company_id = $_SESSION['bill_company_id'];
        }

        if (isset($_SESSION['bill_company_new']))
        {
            $company_new = $_SESSION['bill_company_new'];
        }

        if (isset($_SESSION['bill_category_id']))
        {
            $category_id = $_SESSION['bill_category_id'];
        }

        if (isset($_SESSION['bill_date']))
        {
            $date = $_SESSION['bill_date'];
        }

        if (isset($_SESSION['bill_assurance']))
        {
            $assurance = $_SESSION['bill_assurance'];
        }

        if (isset($_SESSION['bill_description']))
        {
            $description = $_SESSION['bill_description'];
        }

    }
          
?>

<script>
$(function() {
$( "#datepicker" ).datepicker({ dateFormat: 'd.m.yy',
        //changeMonth: true,
        showOn: "button",
        changeYear: true});
});
</script>


<div id="edit">        
    <div id="edit_in">
        <?php $notifi->print_notifi() ?>
        <div class="clearer" ></div>
        
        <form class="create-edit" action="" method="post" accept-charset="UTF-8" enctype="multipart/form-data"> 
            <fieldset>
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <input type="hidden" name="filename" value="<?php echo $file ?>">
                <input type="hidden" name="url" value="<?php echo $url ?>">

                <div class="item">
                        <label> Jméno: </label>
                        <input type="text" name="name" maxlength="100" value="<?php echo $name ?>"> 
                </div>          
                <div class="item"> 
                    <label> Firma: </label>
                    <?php selection_company($user, $company_id) ?>
                    <input id="company" type="text" name="company_new" maxlength="100" title="Zadejte jméno nové firmy" value="<?php echo $company_new ?>"> 
                </div>
                <div class="item">
                    <label> Kategorie: </label>
                    <?php selection_category($category_id) ?>
                </div>
                <div class="item">
                    <label> Datum pořízení: </label>
                    <input type="text" id="datepicker" name="date" value="<?php echo $date; ?>"> 
                </div>
                <div class="item">
                    <label> Záruka: </label>
                    <input type="number" name="assurance" min="0" value="<?php echo $assurance ?>">
                    měsíců
                </div>
                <div class="item">
                    <label> Popis (nepoviný): </label>
                    <textarea name="description" cols="40" rows="3" wrap="soft" ><?php echo $description; ?></textarea>
                </div>
                <div class="item">
                    <label> Nový soubor: </label>
                    <input type="file" name="file" id="file" value="<?php echo $file ?>"> 
                </div>

                <?php 
                if ($file)
                {
                    echo '<div class="item">
                        <label> Název souboru: </label>
                        ' . $file . 
                        '</div>';

                    echo '<div id="image"> 
                        <img src="' . $path . '?' . $data['file'] . '" alt="Obrázek účetnky" width="600" >  
                    </div>';

                }

                if (!is_null($url))
                {
                    echo '
                        <div class="submit">
                            <input id="submit" type="image" name="submit" value="' . SUBMIT_EDIT . '" src="' . HTTP . 'img/create-edit/save.png" alt="Uložit editovanou účtenku">
                            <input type="hidden" name="submit" value="' . SUBMIT_EDIT . '">
                        </div>
                        
                        <div id="back">
                            <a  href="' . HTTP . ACTION_BILL_DETAIL . '/' . $url .'"><img src="' . HTTP . 'img/main/arrow.png" alt="Zpět">Zpět</a>
                        </div>
                    ';
                }
                else {
                    echo '
                        <div class="submit">
                            <input id="submit" type="image" name="submit" value="' . SUBMIT_CREATE . '" src="' . HTTP .'img/create-edit/create.png" alt="Uložit účtenku">
                            <input type="hidden" name="submit" value="' . SUBMIT_CREATE . '">
                        </div>
                        
                        <div id="back">
                            <a  href="' . HTTP . ACTION_LIST . '"><img src="' . HTTP .'img/main/arrow.png" alt="Zpět">Zpět</a>
                        </div>
                    ';
                }
                ?>
                                
            </fieldset>
        </form>
        
    </div>
</div>

<?php

    /**
     * Create tree list of categories
     * @param uint $id id of selected category
     * @return type
     */
    function selection_category($id = NULL)
    {
        $db = Database::get_instance();
        $db->connect();

        $category = DB_Category::get_instance();
        $data = $category->get_categories_root($db);

        echo '<select name="category">';
        $data->data_seek(0);
        while ($row = $data->fetch_assoc()) 
        {
            if ($row['id'] == $id)
            {
                echo '<option value="' . $row['id'] . '" selected = "selected">' . $row['name'] . '</option>';
            }
            else {
                echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
            }

            get_item($db, $category, 1, $row['category_id'], $id);
        }

        echo '</select> ';

        return;

        $db->close();
    }

    /**
     * Create tree list of subcategories for caterory
     * @param Database  $db         Database
     * @param Category  $category   Category class
     * @param uint      $level      level
     * @param uint      $parent_id  parent id
     * @param uint      $id         id of selested item
     */
    function get_item( $db, $category, $level, $parent_id, $id) 
    {
        $data = $category->get_categories_parent($db, $parent_id);

        $s = '';
        for ($i=1; $i<=$level; $i++)
        { 
            $s .= '&nbsp; &nbsp;';  
        }

        $data->data_seek(0);
        while ($row = $data->fetch_assoc()) 
        {
            if ($row['id'] == $id)
            {
                echo '<option value="' . $row['id'] . '" selected = "selected">' . $s . $row['name'] . '</option>';
            }
            else {
                echo '<option value="' . $row['id'] . '">' . $s . $row['name'] . '</option>';
            }

            get_item($db, $category, $level+1, $row['category_id'], $id);
        }
    }
    
    /**
     * Create list of companies belong to user
     * @param User $user    User class
     * @param uint $id      id of selected company
     * @return 
     */
    function selection_company($user, $id = NULL)
    {
        $db = Database::get_instance();
        $db->connect();

        $company = DB_Company::get_instance();
        $data = $company->get_companyies( $db, $user->__get('id'));

        echo '<select name="company">';
        echo '<option value="0">nová firma ->   </option>';

        $data->data_seek(0);
        while ($row = $data->fetch_assoc()) 
        {
            if ($row['id'] == $id)
            {
                echo '<option value="' . $row['id'] . '" selected = "selected">' . $row['name'] . '</option>';
            }
            else {
                echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
            }

        }

        echo '</select> ';

        return;
    } 
                    
?>