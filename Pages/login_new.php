<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */   
 
    if (!isset($_SESSION['login_username']))
    {
        $login_username = 'abc@abc.com';
        $_SESSION['login_username'] = $login_username;
    }
    if (!isset($_SESSION['login_password']))
    {
        $login_password = 'abc@abc.com';
        $_SESSION['login_password'] = $login_password;
    }
    
?>

<script>
	$(function() 
        {
		$( "#dialog-form-login" ).dialog(
                {
                    autoOpen: false,
                    height: 'auto',
                    width: 420,
                    autoResize: true,
                    resizable: false,
                    
                    open: function() {
                        $('button:contains("Přihlášení")').focus();  
                    },
                        
                    close: function() {
                            $( this ).dialog( "close" );
                    }
                    
		});

                $( "#dialog-form-registration" ).dialog(
                {
                    autoOpen: false,
                    height: 'auto',
                    width: 450,
                    autoResize: true,
                    resizable: false,
                    
                    open: function() {
                        $('button:contains("Registrace")').focus();  
                    },
                       
                    close: function() {
                        $( this ).dialog( "close" );     
                    }
		});
                    

                $( ".registration-user" )			
			.click(function() {
				$( "#dialog-form-registration" ).dialog( "open" );
                                
                });
                
		$( "#login-user" )			
			.click(function() {
                                
                                $( "#dialog-form-login" ).dialog( "open" );                                     
                });
                
                
	});
        
        function reg()
        {
            $( "#dialog-form-login" ).dialog( "close" );
            $( "#dialog-form-registration" ).dialog( "open" );
        }
        
        
        function login()
        {
            var data = $('#dialog-form-login form').serialize();

            $.post("router.ajax.php", data, function(d) 
            { 
                console.log(d);
                if (d == "ok")
                { 
                    console.log("dobre");

                    window.location.href = "<?php echo ACTION_LIST ?>";
                }
                else {
                    console.log("spatne");

                    console.log(d.val);

                    $('#list-errors').empty();

                    //$('#list-errors').append(d);
                    $('#list-errors').html(d);
                }
            });                                                                      
        }
        
        function registration()
        {
            var data = $('#dialog-form-registration form').serialize();

            $.post("router.ajax.php", data, function(d) 
            { 
                console.log(d);
                if (d == "ok")
                { 
                    console.log("dobre");

                    window.location.href = "<?php echo ACTION_LIST ?>";
                }
                else {
                    console.log("spatne");

                    console.log(d.val);

                    $('#list-errors2').empty();

                    //$('#list-errors').append(d);
                    $('#list-errors2').html(d);
                }
            });                          
        }
               
</script>



<div id="dialog-form-login" title="Přihlášení do onlineUctenky">
    <form class="forms" action="javascript:login()">
            <div class="columb">
            <div class="label">
		<label for="email">email</label>
            </div>
            <div class="date">
		<input type="text" name="email" id="email" value="<?php echo $_SESSION['login_username'] ?>" class="text ui-widget-content ui-corner-all" />
            </div>
        </div>
        <div class="columb">
            <div class="label">
                <label for="password">heslo</label>
            </div>
            <div class="date">
		<input type="password" name="password" id="password" value="<?php echo $_SESSION['login_password'] ?>" class="text ui-widget-content ui-corner-all" />
            </div>
                <!--<input id="submit" type="image" name="submit" value="<?php //echo SUBMIT_LOGIN ?>" src="img/login/login.png">-->
                <input type="hidden" name="submit" value="<?php echo SUBMIT_LOGIN; ?>">                
	</div>
        
        <button  class="login-button ui-widget-content"> <img src="<?php echo HTTP ?>img/login/login.png" alt="Přihlásit" ></button>
        
        <div class="reg ui-widget-content">
            <a onclick="javascript:reg()">Registrace</a>
	</div>
    </form>
    <br>
    <div id="list-errors">
    </div>
</div>


<div id="dialog-form-registration" title="Registrace do onlineUctenky">	
	<form class="forms" action="javascript:registration()">
            <div class="columb">
            <div class="label_2">
		<label for="email">email</label>
            </div>
            <div class="date">
		<input type="text" name="email" id="email" value="<?php echo $_SESSION['login_username'] ?>" class="text ui-widget-content ui-corner-all" />
            </div>
        </div>
        <div class="columb">
            <div class="label_2">
                <label for="password">heslo</label>
            </div>
            <div class="date">
		<input type="password" name="password" id="password" value="" class="text ui-widget-content ui-corner-all" />
            </div>
        </div>
        <div class="columb">
            <div class="label_2">
                <label for="password">potvrdit heslo</label>
            </div>
            <div class="date">
		<input type="password" name="password_2" id="password_2" value="" class="text ui-widget-content ui-corner-all" />
            </div>
        </div>
                         
                <input type="hidden" name="submit" value="<?php echo SUBMIT_REG; ?>">
                
                <button  class="login-button ui-widget-content"> <img src="<?php echo HTTP ?>img/registration/reg.png" alt="Registrace" ></button>
	
	</form>
    <br>
    <div id="list-errors2">
    </div>
</div>