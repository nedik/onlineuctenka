<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div id="contacts">   
    <div id="contacts_in">    
        <div>
            <h2>Zákaznická podpora</h2>

            <h3>Telefon</h3>
            <div>    
                (+420) 739 555 555
            </div>

            <h3>Email</h3>
            <div>    
                info@onlineuctenka.cz
            </div>

            <h2>Provozuje</h2>
            <div>
                Tomáš Neděla <br>
                Horní Suchá
            </div>
            
            <h3>Vytvořil</h3>
            <div>
                Ing. Radek Zmeškal <br>
                Ostrava
            </div>
        
            <h3>Podminky uzivani</h3>
            <div id="terms">
                <ul>
                    <li> Server slouží primárně k záloze vašich účtenek pro potřeby reklamace.</li>
                
                    <li> Neslouží k uchovávání erotického obsahu a jiného společensky nevhodného obsahu. </li>
                
                    <li> Provozovatel webu se zavazuje zajistit bezpečnost uložených dat a jejich zpracování. </li>
                
                    <li> Užíváním tohoto serveru souhlasíte s těmito podmínkami. </li>
                </ul>
            </div>
        </div>
        
        
        
        <div id="back">
            <a  href="<?php echo HTTP; ?>"><img src="img/main/arrow.png" alt="Zpět">Zpět</a>
        </div>
        
        <div class="clearer" ></div>
        
    </div>
</div>