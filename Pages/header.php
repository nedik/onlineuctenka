<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    require_once 'Classes/User.php';
    require_once 'Database/Database.php';
    require_once 'Database/DB_Category.php';
    
    $user = User::init();
    
    if (!$user->is_logged())
    {
        header("Location: " . HTTP . ACTION_FRONT);
        exit;
    }
    
    $caption = '';
    $h1 = '';    
    
    $parser = Parser::get_instance();
    $action = $parser->get_action();    
    
//    if ($action == ACTION_MAIN)
//    {
//        $caption = '
//            <a href="' . HTTP . ACTION_LIST . '" alt="home" >Seznam uctenek</a>';
//        
//        $h1 = '<h1> Seznam uctenek </h1>';
//    }
    
    if ($action == ACTION_BILL_DETAIL)
    {
        $caption = '
            <a href="' . HTTP . ACTION_LIST . '" alt="Home" >Home</a>
            >  Detail účtenky';
        $h1 = '<h1> Detail účtenky </h1>';
    }
    if ($action == ACTION_BILL_EDIT)
    {
        $array = $parser->get_array();
        $caption = '
            <a href="' . HTTP . ACTION_LIST . '" alt="Home" >Home</a>
            > <a href="' . HTTP . ACTION_BILL_DETAIL . '/' . end($array) . '" alt="home" >Detail</a>
            >  Editace účtenky';
        $h1 = '<h1> Editace účtenky </h1>';
    }
    if ($action == ACTION_BILL_CREATE)
    {
        $caption = '
            <a href="' . HTTP . ACTION_LIST . '" alt="Home" >Home</a>
            > Vytvoření účtenky';
        $h1 = '<h1> Vytvoření účtenky </h1>';
    }
    
    if ($action == ACTION_SETTINGS)
    {
        $count = 1;
        $caption = '
            <a href="' . HTTP . ACTION_LIST . '" alt="Home" >Home</a> 
                > <a href="' . HTTP . ACTION_SETTINGS . '" >Nastavení</a>';
        
        $h1 = '<h1> Nastavení </h1>';
    }       
    
    if ($action == ACTION_LIST)
    {
        $count = 1;
        $caption = '
            <a href="' . HTTP . ACTION_LIST . '" alt="Home" >Home</a>'
            . search_list( $parser, $count);
        
        $h1 = '<h1> Seznam účtenek </h1>';
    }
    
    if ($action == ACTION_SEARCH)
    {
        $count = 1;
        $caption = '
            <a href="' . HTTP . ACTION_LIST . '" alt="Home" >Home</a> '
                . search_list( $parser, $count) . ' > <a href="' . HTTP . ACTION_SEARCH . '/' . $parser->get_search() . '" > vyhledávání "'. $parser->get_search() .'" </a>';
        
        $h1 = '<h1> Vyhledávání </h1>';
    }
    
    /**
     * search for categoryies to write
     * @param Parser    $parser     Parser 
     * @return string
     */
    function search_list( $parser)
    {
        $array = $parser->get_array();
        if (!$array)
        {
            return;
        }
        
        $ret = '';
        $url_n = HTTP . ACTION_LIST;
        
        foreach ($array as $i => $row)
        {   
            $url_n .= '/' . $row['url'];
            
            $ret .= ' > <a href="' . $url_n . '" >'. $row['name'] .' </a>';
        }
        
        return $ret;
    }
    
    if ($action == ACTION_COMPANY)
    {
        $caption = '
            <a href="' . HTTP . ACTION_LIST . '" alt="Home" >Home</a>
                > Seznam uložených firem';
        
        $h1 = '<h1> Seznam firem </h1>';
    }
    
    if ($action == ACTION_CONTACTS)
    {
        $caption = '
            <a href="' . HTTP . ACTION_LIST . '" alt="Home" >Home</a>
                > Kontakty';
        
        $h1 = '<h1> Kontakty </h1>';
    }

?>

<div id="header">
    <div id="header_top">
        <div id="header_top_in" >

            <span class="home">
                <a href="<?php echo HTTP . ACTION_LIST ?>" ><img src="<?php echo HTTP ?>img/onlineUctenka.png" alt="onlineÚčtenka" ></a>
            </span>

            <div class="menu">
                <ul>                
                    <li>
                        <a href="<?php echo HTTP . ACTION_SETTINGS ?>" >nastavení</a>
                    </li>
                    <li>
                        <a href="<?php echo HTTP . ACTION_CONTACTS ?>" >kontakty</a>
                    </li>
                    <li>
                        <a  href="<?php echo HTTP . ACTION_COMPANY ?>" >firmy</a>
                    </li>
                    <li>
                        <a  href="<?php echo HTTP . ACTION_LIST ?>" >domů</a>
                    </li>
                    
                </ul>
            </div>
        </div>
    </div>
    
    <div class="clearer" ></div>
    
    <div id="header_bot">
        <div id="header_bot_in">
            <div class="left">
                <div id="links">
                    <?php echo $caption; ?>
                </div>
               <?php echo $h1; ?>
                
            </div>     
            <div class="right">
                <div class="menu">
                    <ul>                
                        <li>
                            <a href="index.php?submit=<?php echo SUBMIT_LOGOUT ?>" >odhlásit</a>
                        </li>
                        <li>
                            <?php echo $user->__get('email'); ?>
                        </li>
                    </ul>
                </div>
                <div >
                    <form class="search"  action="" method="post" accept-charset="UTF-8"> 
                            <fieldset>
                                
                                    <input type="image" name="submit" value="<?php echo SUBMIT_SEARCH ?>" src="<?php echo HTTP ?>img/main/search.png">
                                    <input type="hidden" name="submit" value="<?php echo SUBMIT_SEARCH ?>">
                                    <input class="text" type="text" name="search" maxlength="40" value=""> 
                                
                            </fieldset>
                    </form> 
                </div>
                
            </div>
        </div>
    </div>
    <div class="clearer" ></div>
</div>