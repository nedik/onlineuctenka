<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div id="page_404">        
    <div id="page_404_in">
        
            <h2>Omlouváme se, ale požadovaná stránka nebyla nalezena</h2>
            
            <a href="<?php echo HTTP . ACTION_LIST; ?>" alt="Hlavni stranka">  <h3> Pokracujte na hlavni stranku </h3> </a>
            
            <div id="bill">
                <img src="<?php echo HTTP ?>img/404_bill.png" alt="404" >
            </div>
            
            <div id="cause">
                Možné důvody:
                <ol>
                    <li>Špatná url adrsa.</li>
                    <li>Účet neexistuje, kontaktuje prosím administrátora.</li>
                </ol>
            </div>
            
<!--            pokud ho slozite a poslete nazem dostanete nevyznamny bod-->
            
            <div class="clearer" ></div>  
                       
        <?php $notifi->print_notifi() ?>
        <div class="clearer" ></div>
    </div>
</div>

<?php 
    require_once 'footer.php';
?>