<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    require_once('Database/Database.php');
    require_once('Database/DB_Category.php');
    require_once('Classes/User.php');
    
    $user = User::init();
    
    $db = Database::get_instance();
    $db->connect();
    
    $parser = Parser::get_instance();
    
     get_catalog($db, $user->__get('id'));
     
     $db->disconnect();
     
     return;
         
     /**
     * Create tree catalog for categoryies
     * @param Database  $db         Database
     * @param uint      $id_user    id of user
     */
    function get_catalog($db, $id_user)
    {   
        $search = null;
        $parser = Parser::get_instance();
        $array = $parser->get_array();
        if ($parser->get_action() == ACTION_SEARCH)
        {
            $search = $parser->get_search();
        } 
        
        $catalog = DB_Catalog::get_instance();
        $data = $catalog->get_categoryies($db, $id_user , 0, $search);                
        
        $count = 0;
        
        echo '<ul>';
        while ($row = $db->fetch_array($data)) 
        {   
            if (is_null($search))
            {
                $url = HTTP . ACTION_LIST . '/' . $row['url'];
                $url_n = $url;
            }
            else {
                $url = HTTP . ACTION_SEARCH . '/' . $row['url'];
                $url_n = $url . '?q=' . $search;
            }

            if (is_null($array))
            {
                echo '<li> <a href="' . $url_n . '" >' . $row['name'] . ' (' . $row['count'] . ')</a>';
                continue;
            }
            if ($array[$count]['id'] == $row['id'])
            {                
                echo '<li> <a class="roll" href="' . $url_n . '" >' . $row['name'] . ' (' . $row['count'] . ')</a>';
                search_childs($db, $catalog, $id_user, $array, $url, $count, $row['category_id'], $search); 
            }
            else {
                echo '<li> <a href="' . $url_n . '" >' . $row['name'] . ' (' . $row['count'] . ')</a>';
            }
            
            echo '</li>';
        }
        echo '</ul>'; 
    }

    /**
     * Create tree catalog of subcategoryies for categoryies
     * @param Database      $db             Database
     * @param Catalog       $catalog        Catalog class
     * @param uint          $id_user        id of user
     * @param array[string] $array          array of selected categoryies
     * @param string        $url_n          new url
     * @param uint          $count          count of processing parrent categories
     * @param uint          $category_id    category id
     * @param string        $search         search parameters
     */
    function search_childs($db, $catalog, $id_user, $array, $url_n, $count, $category_id, $search) 
    {
        $count += 1;
        
        $data = $catalog->get_categoryies($db, $id_user, $category_id, $search);
        
        echo '<ul>';
        
        $url = '';
        while ($row = $db->fetch_array($data)) 
        {
            if (is_null($search))
            {                        
                $url = $url_n . '/' . $row['url'];
                $url_nn = $url;
            }
            else {
                $url = $url_n . '/' . $row['url'];
                $url_nn = $url . '?q=' . $search;
            }            
            
            if ((count($array) <= $count))
            {
                echo '<li> <a href="' . $url_nn . '" >' . $row['name'] . ' (' . $row['count'] . ')</a></li>';
                continue;
            }
            if ($array[$count]['id'] == $row['id'])
            {
                echo '<li> <a class="roll" href="' . $url_nn . '" >' . $row['name'] . ' (' . $row['count'] . ')</a>';
                
                search_childs($db, $catalog, $id_user, $array, $url, $count, $row['category_id'], $search); 
                
                echo '</li>';
            }
            else {
                echo '<li> <a href="' . $url_nn . '" >' . $row['name'] . ' (' . $row['count'] . ')</a></li>';
            }
        }
        echo '</ul>'; 
    }
?>
