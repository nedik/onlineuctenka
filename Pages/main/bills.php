<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
    require_once('Classes/User.php');
    require_once('Database/Database.php');
    require_once('Database/DB_Bill.php');
    
        
    $user = User::init();
    
    if (!$user->is_logged())
    {
        header("Location: " . HTTP . ACTION_LOGIN);
        exit;
    }
    
    $parser = Parser::get_instance();
    
    $id = NULL;
    $search= null;
    $array = $parser->get_array();       
    $action = $parser->get_action();
    
    if ($action == ACTION_SEARCH)
    {        
        $search = $parser->get_search();   
        if ($array)
        {
            $array = end($array);
            $id = $array['id'];
        }
    }
    else {
        if ($array)
        {
            $array = end($array);
            $id = $array['id'];
        }
    }
    
    
    $db = Database::get_instance();
    $db->connect();
        
    $bills = DB_Bill::get_instance();
    
    $max = $bills->get_rows_count( $db, $user->__get('id'), $id, $search);    
    
    $page = $parser->get_page();
    
    $max_page = ceil($max / ON_PAGE);
        
    if ($max_page <= 0)
    {
        $max_page = 1;
    }
    
    if ($page > $max_page)
    {
        $page = $max_page;
    }
        
    $start = ($page-1) * ON_PAGE;        
    
    $data = $bills->get_bills( $db, $user->__get('id'), $id, $parser->get_order(), $parser->get_order_by(), $search, $start, ON_PAGE);
        
    $db->disconnect();
    
    if (is_null($data))
    {
        exit;
    }
    
    while ($row = $db->fetch_array($data)) 
    {
        echo '<div class="bill">';
        echo '<div class="name">';
        echo $row['name'];
        echo '</div>';
        
        echo '<div class="company">';
        echo $row['comp_name'];
        echo '</div>';
        
        echo '<div class="date">';
        echo  date( 'd. n. Y', strtotime($row['date']));
        echo '</div>';
        
        echo '<div class="detail">';
        echo '<a href="' . HTTP . ACTION_BILL_DETAIL . '/' . $row['url'] . '"> <img src="' . HTTP . 'img/main/detail.png" alt=""> </a>';
        echo '</div>';  
        
        echo '<div class="clearer" ></div>';
         
        echo '</div>';
    }
    
    echo '<div class="listing">';
    
    $url_s = HTTP . $parser->get_action() .  $parser->get_url() . '?' . ACTUAL_PAGE . '=';
    $url_e = '';
    
    if (!is_null($parser->get_search()))
    {
        $url_e .= '&q=' . $parser->get_search();
    }
    if (!is_null($parser->get_order()))
    {
        $url_e .= '&' . ORDER . '=' . $parser->get_order();
    }
    if (!is_null($parser->get_order_by()))
    {
        $url_e .= '&' . ORDER_BY . '=' . $parser->get_order_by();
    }
    
    if($page > 1) 
    {
        echo '<a href="' . $url_s . '1' . $url_e . '">&lt;&lt;</a>';
    }

    if($page > 1) 
    { 
          echo '<a href="' . $url_s . ($page - 1) . $url_e . '">&lt;</a>';

          for($i = 4; $i > 0; $i--) 
          {
                if(($page - $i) >= 1){
                    echo '<a href="' . $url_s . ($page - $i) . $url_e . '">' . ($page - $i) . '</a>';
                }
          }
    }
    
    if ($max_page > 1)
    {
        echo $page;
    }
    
    
    if($page < $max_page) 
    {
        for($i = 1; $i < 4; $i++) 
        { 
            if(($page + $i) <= $max_page) 
            {
                echo '<a href="' . $url_s . ($page + $i) . $url_e . '">' . ($page + $i) . '</a>';
            }
        }
        echo '<a href="' . $url_s . ($page + 1) . $url_e . '">&gt;</a>';
    }
    
    if($page < $max_page) 
    {
        echo '<a href="' . $url_s . ceil($max / ON_PAGE) . $url_e  . '">&gt;&gt;</a>';
    }
    
    echo '</div>';
    
    
    return;

?>
