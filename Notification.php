<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('Settings/definitions.php');
require_once 'Logger.php';


/**
 * Class to evidence error messages
 */
class Notification
{
    private static $instance = NULL;
    private $notifications = array() ;


    /**
     * Constructor
     */
    public function __construct()
    {
        if(isset($_SESSION['notifications']))
        {
            $this->notifications = $_SESSION['notifications'];
        }
        
    }
    
    /**
     * Destructor
     */
    public function __destruct()
    {     
        $_SESSION['notifications'] = $this->notifications;
    }
    
    /**
     * Return instance of class
     * @return instance of class Notification
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) 
        {
            self::$instance = new $class;
        }
        
        return self::$instance;
    }
    
    /**
     * Inicialization of notification
     */
    public function init()
    {
        foreach ($this->notifications as $i => &$row)
        {
            if ($row[1] >= 2)
            {
                unset($this->notifications[$i]);
            }
            else {
                $row[1] += 1;
            }
        }        
    }
    
    /**
     * Inicialization of notification for string
     */
    public function init_str()
    {
        foreach ($this->notifications as $i => &$row)
        {
            if ($row[1] >= 1)
            {
                unset($this->notifications[$i]);
            }
            else {
                $row[1] += 1;
            }
        }        
    }

    /**
     * Set new messages 
     * @param int       $type       id of error
     * @param string    $message    text of message
     */
    public function add( $type, $message)
    {
//        $this->notifications = $_SESSION['notifications'];
        $error = Errors::get_instance();
        
//        $var = print_r($this->notifications, true);
//        $error->add( ERROR_TEST, 'session ' . $var);
        
        $this->notifications[] = array( $message, 0 , $type);
        
//        $var = print_r($this->notifications, true);
//        $error->add( ERROR_TEST, 'session ' . $var);
        
//        $_SESSION['notifications'] = $this->notifications;
        
        $logger = Logger::get_instance();
        $var = print_r($this->notifications, true);
//        $var = 'print_r($_SESSION[\'notifications\'], true)';
        $logger->add( time(), 100000, debug_backtrace(), $var);
        
        
//        $var = print_r($_SESSION['notifications'], true);
//        $error->add( ERROR_TEST, 'session1 ' . $var);
    }  
    
    public function print_notifi()
    {   
        $this->init();
        $logger = Logger::get_instance();
        //$var = print_r($this->notifications, true);
        $var = 'print_r($this->notifications, true)';
        //$logger->add( time(), 9999999999999, debug_backtrace(), $var);
        
        if (count($this->notifications) == 0)
        {
            return;
        }
        
        echo '<div class="notification">';
        
        foreach ($this->notifications as $i => $row)
        {
            if ($row[2] == NOTIFI_NORMAL)
            {
                echo '<div class="normal">' . $row[0] . '</div>';
            }
            
            if ($row[2] == NOTIFI_ERROR)
            {
                echo '<div class="error">' . $row[0] . '</div>';
            }
            
        }
        echo '</div>';
        
    }
    
    public function get_str()
    {     
        $this->init_str();
        //echo session_encode();
        if(isset($_SESSION['notifications']))
        {
//            $this->notifications = $_SESSION['notifications'];
        }
        
        $logger = Logger::get_instance();
        $var = print_r($this->notifications, true);
 //       $var = 'print_r($this->notifications, true)';
//        $logger->add( time(), 9999999999999, debug_backtrace(), $var);
              
        //echo '<p>' . $var . '</p>';
        //echo 'test count ' . count($this->notifications). '<br>';
        
        $str = '';
        
        if (count($this->notifications) == 0)
        {
            return $str;
        }
        
        $str .= '<div class="notification">';
        
        foreach ($this->notifications as $i => $row)
        {
            $row[1] += 2;
            //echo $row[0] . ' </br>';
            if ($row[2] == NOTIFI_NORMAL)
            {
                $str .= '<div class="normal">' . $row[0] . '</div>';
            }
            
            if ($row[2] == NOTIFI_ERROR)
            {
                $str .= '<div class="error">' . $row[0] . '</div>';
            }
            
        }
        $str .= '</div>';
        
        return $str;
    }
    
    public function get_str_text()
    {     
        $this->init_str();
        
        $logger = Logger::get_instance();
        $var = print_r($this->notifications, true);
 //       $var = 'print_r($this->notifications, true)';
        
        $str = '';
        
        if (count($this->notifications) == 0)
        {
            return $str;
        }
        
        foreach ($this->notifications as $i => $row)
        {
            if ($row[2] == NOTIFI_NORMAL)
            {
                $str .= $row[0] . "\n";
            }
            
            if ($row[2] == NOTIFI_ERROR)
            {
                $str .= $row[0] . "\n";
            }
            
        }
        
        return $str;
    }
    
}


?>
