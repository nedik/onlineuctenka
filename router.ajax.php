<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

    require_once 'Classes/User.php';

    session_start();

    $user = User::init();
    
    $notifi = Notification::get_instance();
    
    $ret = false;
    
    if(isset($_POST['submit']))
    {
            if ($_POST['submit'] == SUBMIT_LOGIN) 
            {    
                $ret =  $user->login();
                //print_r($_SESSION);                
            }
            
            if ($_POST['submit'] == SUBMIT_REG) 
            {    
                $ret =  $user->registration();
                //print_r($_SESSION);                
            }
    }
            
    $value = "";
    if ($ret)
    {
        $value = 'ok';
    }
    else {
        $n = $notifi->get_str();
        //$n = addslashes($n);
        //echo $n;
        $value = $n;
    }
    
    echo $value;
    return $value;

?>
