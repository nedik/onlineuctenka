<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'Settings/conf.php';

/**
 * Class to working with files
 */
class File_manager 
{
    private static $instance = NULL;

    /**
     * Constructor
     */
    function __construct() {
        ;
    }
    
    /**
     * Destructor
     */
    function __destruct() {
        ;
    }
    
    /**
     * Return instance of class
     * @return instance of class File_manager
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) {
            self::$instance = new $class;
        }
        return self::$instance;
    }
    
    /**
     * Create folder in directory from conf.php
     * @param string $name  name of directory 
     * @return boolean  foleder is cereated
     */
    public function create_folder($name)
    {
        $path = conf::get_path() . '/' . $name;    
        
        if (!is_dir($path))
        {
            mkdir($path);
            return TRUE;
        }
        
        return FALSE;
    }
    
    /**
     * Get extension from filename
     * @param string $filename  name of file
     * @return string; extension
     */
    public function get_extension($filename)
    {
        //$extension = end(explode(".", $_FILES["file"]["name"]));
        //$extension = substr(strrchr($_FILES['file']['name'], "."), 1);
        
        $rex = NULL;
        preg_match('/.*\.([a-zA-Z]{3,4})$/', $filename, $rex);

        $extension = end($rex);
        
        if ($extension)
        {
            return $extension;
        }
        else {
            return NULL;
        }
        
        
    }
    
    /**
     * Testing input file from $_FILES
     * @param FILE $file    file from $_FILES
     * @return boolean      file is OK
     */
    public function test_file($file)
    {
        $errors = Errors::get_instance();
        $notifi = Notification::get_instance();
        
        if (($file["error"] > 0)) 
        {
            if ($file["error"] != 4)
            {
                $errors->add( ERROR_IMAGE, 'Return Code: ' . $file["error"]);
                $notifi->add( NOTIFI_ERROR, 'Chyba nahraváného obrázků, označení '. $file["error"]);
            }

            return FALSE;
        }
        return TRUE;
    }

    /**
     * Testing input files if user load right files
     * @param FILE $file
     * @return boolean
     */
    public function test_input_file( $file)
    {    
        $errors = Errors::get_instance();
        
        $notifi = Notification::get_instance();
        
        if (!$this->test_file($file)) 
        {   
            return FALSE;
        }
        
        $extension = $this->get_extension($file['name']);
        
        $extensions = array("jpg", "jpeg", "gif", "png", 'pdf');

        if (!((($file["type"] == "image/gif") || ($file["type"] == "image/jpeg") || ($file["type"] == "image/png") /*|| ($file["type"] == "application/pdf")*/) && in_array($extension, $extensions))) 
        {
            $errors->add( ERROR_REGISTR_DATA, 'You did not upload image file');
            $notifi->add( NOTIFI_NORMAL, 'Chyba zadaného souboru, neopovídá požadavkům');
            return FALSE;
        }
        
        return TRUE;
    }
    
    /**
     * Save file to user directory
     * @param string    $file      FILE
     * @param uint      $user_id   id of user
     * @param string    $filename  new name of file
     * @return boolean  file saved
     */
    public function save_file( $file, $user_id, $filename)
    {   
        $errors = Errors::get_instance();
        
        $notifi = Notification::get_instance();
        $logger = Logger::get_instance();
        
        $path = conf::get_path() . '/' . $user_id;
        
        if (!is_dir($path))
        {
            $errors->add( ERROR_INSERT_IMAGE, $path . ' dont exists. ');
            $notifi->add( NOTIFI_ERROR, 'Adresář pro uložení neexistuje');
            $logger->add( time(), ERROR_INSERT_IMAGE, debug_backtrace(), $path . ' dont exists');
            
            return FALSE;
        }
        
        $extension = $this->get_extension($file['name']) ;
        
        $file_path = $path . '/' . $filename . '.' . $extension;
        
        if (file_exists($file_path)) 
        {
            $errors->add( ERROR_INSERT_IMAGE, $file_path . ' already exists. ');
            $notifi->add( NOTIFI_ERROR, 'Účtenka na serveru už existuje');
            $logger->add( time(), ERROR_INSERT_IMAGE, debug_backtrace(), $file_path . ' already exists. ');
            
            return FALSE;
        } 

        $result =  move_uploaded_file( $file["tmp_name"], $file_path);

        if (!$result)
        {
            $errors->add( ERROR_INSERT_IMAGE, 'Not stored in: ' . $file_path);
            $notifi->add( NOTIFI_ERROR, 'Účtenka se neuložila');
            $logger->add( time(), ERROR_INSERT_IMAGE, debug_backtrace(), 'Not stored in: ' . $file_path);
            
            return FALSE;
        }
        
        return TRUE;
    }
    
    /**
     * Replace file with new file
     * @param FILE      $file       FILE
     * @param uint      $user_id    id of user
     * @param string    $filename   new name of ile
     * @return boolean  file is replaced
     */
    public function update_file( $file, $user_id, $filename)
    {   
        $errors = Errors::get_instance();
        $notifi = Notification::get_instance();
        $logger = Logger::get_instance();
        
        $path = conf::get_path() . '/' . $user_id;
        
        if (!is_dir($path))
        {
            $errors->add( ERROR_INSERT_IMAGE, $path . ' already exists. ');            
            $notifi->add( NOTIFI_ERROR, 'Adresář pro uložení neexistuje');
            $logger->add( time(), ERROR_INSERT_IMAGE, debug_backtrace(), $path . ' already exists. ');
            
            return FALSE;
        }
        
        $extension = $this->get_extension($file['name']) ;
        
        $file_path = $path . '/' . $filename . '.' . $extension;

        $result =  move_uploaded_file( $file["tmp_name"], $file_path);

        if (!$result)
        {
            $errors->add( ERROR_INSERT_IMAGE, 'Not stored in: ' . $file_path);            
            $notifi->add( NOTIFI_ERROR, 'Účtenka se neuložila');
            $logger->add( time(), ERROR_INSERT_IMAGE, debug_backtrace(), 'Not stored in: ' . $file_path);
            
            return FALSE;
        }
        
        return TRUE;
    }     
}
?>
