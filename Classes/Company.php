<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



/**
 * Class to work with comapanyies
 */
class Company 
{
    private static $instance = NULL;

    /**
     * constructor
     */
    function __construct() {
        ;
    }
    
    /**
     * destructor
     */
    function __destruct() {
        ;
    }
    
    /**
     * Return instance of class
     * @return Bill instance class of company
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) {
            self::$instance = new $class;
        }
        return self::$instance;
    }
       
    /**
     * Create new comapny
     * @param User $user user class
     */
    public function create_company($user) 
    {
        $errors = Errors::get_instance();        
        $notifi = Notification::get_instance();

        if (!$_POST['name']) 
        {
            $errors->add( ERROR_REGISTR_DATA, 'You did not fill in a required field');
            $notifi->add( NOTIFI_NORMAL, 'Nejsou vyplněné potřebné data.');

            header( "Location: " . HTTP . ACTION_COMPANY);
            exit;
        }
        
        $name = $_POST['name'];
                    
        $db = Database::get_instance();
        $db->connect();
        
        $company = DB_Company::get_instance();              
        $result = $company->insert( $db, $user->__get('id'), $name);
        
        if (!$result)
        {
            $errors->add( ERROR_SELECT, 'Bill with this company exist');
            $notifi->add( NOTIFI_NORMAL, 'Nejde smazat, existuje účet s touto firmou');
            
            $db->disconnect();
            header( "Location: " . HTTP . ACTION_COMPANY);            
            exit;
        }
        
        $db->disconnect();
        header( "Location: " . HTTP . ACTION_COMPANY);            
        exit;                
    }
    
    /**
     * Delete bill
     * @param type $user
     */
    public function delete_company($user) 
    {
        $errors = Errors::get_instance();
        $notifi = Notification::get_instance();
        
        if ( !$_GET['id'] ) 
        {
            $errors->add( ERROR_BILL_DATA, 'You did not fill in a required field');
            $notifi->add( NOTIFI_NORMAL, 'Nejsou vyplněné potřebné data.');

            header("Location: " . HTTP . ACTION_COMPANY);
            exit;
        } 
        
        $id = $_GET['id'];
                    
        $db = Database::get_instance();
        $db->connect();
        
        $company = DB_Company::get_instance();              
        $result = $company->delete( $db, $user->__get('id'), $id);
                    
        if (!$result)
        {
            $errors->add( ERROR_DELETE, 'Error delete company');
            $notifi->add( NOTIFI_NORMAL, 'Chyba vymazání firmy');
            
            $db->disconnect();
            header( "Location: " . HTTP . ACTION_COMPANY );            
            exit;
        }
        
        $db->disconnect();
        header( "Location: " . HTTP . ACTION_COMPANY);          
    }


}
?>