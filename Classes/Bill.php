<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'Errors.php';
require_once 'Classes/File_manager.php';
require_once 'Classes/User.php';
require_once 'Settings/conf.php';
require_once 'Database/Database.php';
require_once 'Database/DB_Bill.php';
require_once 'Database/DB_Company.php';
require_once 'Database/DB_Catalog.php';

/**
 * Class to work with bills
 */
class Bill 
{
    private static $instance = NULL;

    /**
     * constructor
     */
    function __construct() {
        ;
    }
    
    /**
     * destructor
     */
    function __destruct() {
        ;
    }
    
    /**
     * Return instance of class
     * @return Bill instance class of bill
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) {
            self::$instance = new $class;
        }
        return self::$instance;
    }
       
    /**
     * Create new bill
     * @param User $user user class
     */
    public function create_bill($user) 
    {
        $errors = Errors::get_instance();
        $notifi = Notification::get_instance();
        
        $file_manager = File_manager::get_instance();
        
        if (isset($_POST['name']))
        {
            $_SESSION['bill_name'] = $_POST['name'];
        }

        if (isset($_POST['date']))
        {
            $_SESSION['bill_date'] = $_POST['date'];
        }

        if (isset($_POST['category']))
        {
            $_SESSION['bill_category_id'] = $_POST['category'];
        }

        if (isset($_POST['company']))
        {
            $_SESSION['bill_company_id'] = $_POST['company'];
        }

        if (isset($_POST['company_new']))
        {
            $_SESSION['bill_company_new'] = $_POST['company_new'];
        }

        if (isset($_POST['assurance']))
        {
           $_SESSION['bill_assurance']  = $_POST['assurance'];
        }

        if (isset($_POST['description']))
        {
            $_SESSION['bill_description'] = $_POST['description'];
        }
            

        $file = $_FILES['file'];
        if (!$file_manager->test_input_file($file))
        {
            $errors->add(ERROR_BILL_DATA, 'You did not load required image');
            $notifi->add(NOTIFI_NORMAL, 'Špatný typ obrázku');
            header("Location: " . HTTP . ACTION_BILL_CREATE);
            exit;
        }
        
        if ( !$_POST['name'] || !$_POST['date'] || !$_POST['category'] || !$_POST['assurance'] || (($_POST['company'] == 0) && !$_POST['company_new'])) 
        {
            $errors->add(ERROR_BILL_DATA, 'You did not fill in a required field');
            $notifi->add(NOTIFI_NORMAL, 'Nejsou vyplněné potřebné data.');

            header("Location: " . HTTP . ACTION_BILL_CREATE);
            exit;
        } 
        
        $db = Database::get_instance();
        $db->connect();

        $user_id = $user->__get('id');
        $company_id = NULL;
        if ($_POST['company'] == 0) 
        {
            $company = DB_Company::get_instance();
            $result = $company->insert($db, $user_id, $_POST['company_new']);
            if (!$result)
            {
                $errors->add(ERROR_INSERT, 'Company already exist');
                $notifi->add(NOTIFI_NORMAL, 'Firma pod stejným názvem už existuje.');

                $db->disconnect();
                header("Location: " . HTTP . ACTION_BILL_CREATE);
                exit;
            }
            
            $company_id = $db->get_insert_id();
        }
        else {
            $company_id = $_POST['company'];
        }
        
        $name = $_POST['name'];
        $category_id = $_POST['category'];
        $date = $_POST['date'];
        $date = date('Y-m-d', strtotime($date));
        $assurance = $_POST['assurance'];
        $description = $_POST['description'];

        $bill = DB_Bill::get_instance();

        $result = $bill->insert($db, $user_id, $company_id, $category_id, $name, $date, $assurance, $description, $file['name']);

        if (!$result) 
        {
            $errors->add(ERROR_REGISTR, 'Error create new bill');
            $notifi->add(NOTIFI_NORMAL, 'Chyba vytvoření nového účtu.');

            $db->disconnect();
            header("Location: " . HTTP . ACTION_BILL_CREATE);
            exit;
        }
        
        $id = $db->get_insert_id();
        
        $result = $file_manager->save_file( $file, $user_id, $id);
        
        if (!$result)
        {
            $errors->add(ERROR_REGISTR, 'Error create new bill, save file');
            $notifi->add(NOTIFI_NORMAL, 'Chyba vytvoření nového účtu, uložení souboru.');
            
            $bill->delete( $db, $id);
            header("Location: " . HTTP . ACTION_BILL_CREATE);
            $db->disconnect();
            exit;
        }
        
        $catalog = DB_Catalog::get_instance();
        $result = $catalog->insert( $db, $user_id, $category_id, $id);
        
        if (!$result) 
        {
            $errors->add(ERROR_REGISTR, 'Error create new bill, create catalog');
            $notifi->add(NOTIFI_NORMAL, 'Chyba vytvoření nového účtu, vytvoření katalogu');
            
            $db->disconnect();
            header("Location: " . HTTP . ACTION_BILL_CREATE);
            exit;
        }

        $db->disconnect();
        
        unset($_SESSION['bill_name']);
        unset($_SESSION['bill_date']);
        unset($_SESSION['bill_category_id']);
        unset($_SESSION['bill_company_id']);
        unset($_SESSION['bill_company_new']);
        unset($_SESSION['bill_assurance']);
        unset($_SESSION['bill_description']);
        
        header("Location: " . HTTP . ACTION_LIST);
        exit;
    }
    
    /**
     * Create new bill
     * @param User $user user class
     */
    public function create_bill_android($user_id) 
    {
        $errors = Errors::get_instance();
        $notifi = Notification::get_instance();
        
        $file_manager = File_manager::get_instance();        
            
        $file = $_FILES['file'];
        if (!$file_manager->test_input_file($file))
        {
            $errors->add(ERROR_BILL_DATA, 'You did not load required image');
            $notifi->add(NOTIFI_NORMAL, 'Špatný typ obrázku');
            return false;
        }
        
        if ( !$_POST['name'] || !$_POST['date'] || !$_POST['category'] || !$_POST['assurance'] || !$_POST['company']) 
        {
            $errors->add(ERROR_BILL_DATA, 'You did not fill in a required field');
            $notifi->add(NOTIFI_NORMAL, 'Nejsou vyplněné potřebné data.');

             return false;
        } 
        
        $db = Database::get_instance();
        $db->connect();

        $company_id = NULL;
        if ($_POST['company'] == 0) 
        {
            $company = DB_Company::get_instance();
            $result = $company->insert($db, $user_id, $_POST['company_new']);
            if (!$result)
            {
                $errors->add(ERROR_INSERT, 'Company already exist');
                $notifi->add(NOTIFI_NORMAL, 'Firma pod stejným názvem už existuje.');

                $db->disconnect();
                return false;
            }
            
            $company_id = $db->get_insert_id();
        }
        else {
            $company_id = $_POST['company'];
        }
        
        $name = $_POST['name'];
        $category_id = $_POST['category'];
        $date = $_POST['date'];
        $date = date('Y-m-d', strtotime($date));
        $assurance = $_POST['assurance'];
        $description = $_POST['description'];

        $bill = DB_Bill::get_instance();

        $result = $bill->insert($db, $user_id, $company_id, $category_id, $name, $date, $assurance, $description, $file['name']);

        if (!$result) 
        {
            $errors->add(ERROR_REGISTR, 'Error create new bill');
            $notifi->add(NOTIFI_NORMAL, 'Chyba vytvoření nového účtu.');

            $db->disconnect();
             return false;
        }
        
        $id = $db->get_insert_id();
        
        $result = $file_manager->save_file( $file, $user_id, $id);
        
        if (!$result)
        {
            $errors->add(ERROR_REGISTR, 'Error create new bill, save file');
            $notifi->add(NOTIFI_NORMAL, 'Chyba vytvoření nového účtu, uložení souboru.');
            
            $bill->delete( $db, $id);           
            return false;
        }
        
        $catalog = DB_Catalog::get_instance();
        $result = $catalog->insert( $db, $user_id, $category_id, $id);
        
        if (!$result) 
        {
            $errors->add(ERROR_REGISTR, 'Error create new bill, create catalog');
            $notifi->add(NOTIFI_NORMAL, 'Chyba vytvoření nového účtu, vytvoření katalogu');
            
            $db->disconnect();
             return false;
        }

        $db->disconnect();
        
        return true;
    }
    
    /**
     * Editate bill
     * @param type $user
     */
    public function edit_bill($user) 
    {
        $errors = Errors::get_instance();
        $notifi = Notification::get_instance();
        
        $file_manager = File_manager::get_instance();
                
        if ( !$_POST['id'] || !$_POST['name'] || !$_POST['date'] || !$_POST['category'] || !$_POST['assurance'] || !$_POST['filename'] || !$_POST['url'] || (($_POST['company'] == 0) && !$_POST['company_new'])) 
        {
            $errors->add(ERROR_BILL_DATA, 'You did not fill in a required field');
            $notifi->add(NOTIFI_NORMAL, 'Nejsou vyplněné potřebné data.');

            header("Location: " . HTTP . ACTION_BILL_EDIT);
            exit;
        } 
        
        $id = $_POST['id'];
        
        $db = Database::get_instance();
        $db->connect();
        
        $bill = DB_Bill::get_instance();

        $file = $_FILES['file'];
        $test = $file_manager->test_file($file);
        if ($test)
        {
            if (!$file_manager->test_input_file($file))
            {
                $errors->add(ERROR_BILL_DATA, 'You did not load required image');
                $notifi->add(NOTIFI_NORMAL, 'Špatný typ obrázku');
                exit;
            }
        }

        $user_id = $user->__get('id');
        $company_id = NULL;
        if ($_POST['company'] == 0) 
        {
            $company = DB_Company::get_instance();
            $company->insert($db, $user_id, $_POST['company_new']);
            $company_id = $db->get_insert_id();
        }
        else {
            $company_id = $_POST['company'];
        }
        
        $name = $_POST['name'];
        $category_id = $_POST['category'];
        $date = $_POST['date'];
        $date = date('Y-m-d', strtotime($date));
        $assurance = $_POST['assurance'];
        $description = $_POST['description'];
        $file_name = $_POST['filename'];
        $url = $_POST['url'];
        
        if ($test) 
        {
            $file_name = $file['name'];
            $result = $file_manager->update_file( $file, $user_id, $id);
            if (!$result)
            {
                $errors->add( ERROR_UPDATE, 'Error update bill, overwrite file');                
                $notifi->add(NOTIFI_NORMAL, 'Chyba aktualizace účtu, přepsání obrázku');
                
                $db->disconnect();
                header("Location: " . HTTP . ACTION_BILL_EDIT . '/' . $url);
                exit;
            }
        }
        
        
        $result = $bill->update($db, $id, $user_id, $company_id, $category_id, $name, $date, $assurance, $description, $file_name);

        if (!$result) 
        {
            $errors->add( ERROR_UPDATE, 'Error update bill');            
            $notifi->add( NOTIFI_NORMAL, 'Chyba aktualizace účtu.' );

            $db->disconnect();
            header("Location: " . HTTP . ACTION_BILL_EDIT . '/' . $url);
            exit;
        }
        
        $catalog = DB_Catalog::get_instance();
        $result = $catalog->update( $db, $user_id, $category_id, $id);
        
        if (!$result) 
        {
            $errors->add( ERROR_UPDATE, 'Error create new catalog');
            $notifi->add( NOTIFI_NORMAL, 'Chyba aktualizace účtu, vytvoření nového katalogu');

            $db->disconnect();
            header("Location: " . HTTP . ACTION_BILL_EDIT . '/' . $url);
            exit;
        }
        
        $db->disconnect();
        header("Location: " . HTTP . ACTION_BILL_DETAIL . '/'. $url);
        exit;
    }
    
    /**
     * Delete bill
     * @param type $user
     */
    public function delete_bill($user) 
    {
        $errors = Errors::get_instance();
        $notifi = Notification::get_instance();
        
        if ( !$_GET['id'] ) 
        {
            $errors->add(ERROR_BILL_DATA, 'You did not fill in a required field');
            $notifi->add(NOTIFI_NORMAL, 'Nejsou vyplněné potřebné data.');

            header("Location: " . HTTP);
            exit;
        } 
        
        $url = $_GET['id'];

        $db = Database::get_instance();
        $db->connect(); 
        
        $bill = DB_Bill::get_instance();
        $result = $bill->get_bill( $db, $url);
        
        if (!$result)
        {
            $errors->add( ERROR_SELECT, 'Select bill ' . $id);    
            $notifi->add(NOTIFI_NORMAL, 'Účtenka neexistuje');
            return NULL;
        }
        
        $data = $db->fetch_array($result);
        if (!$data)
        {
            $errors->add( ERROR_SELECT, 'Select no bill ' . $id);
            $notifi->add(NOTIFI_NORMAL, 'Účtenka neexistuje');
            return NULL;
        }
        
        $id = $data['id'];
        
        if ($data['id_user'] != $user->__get('id'))
        {
            $errors->add( ERROR_ID, 'Wrong id user ' . $data['id_user'] . '!=' . $user->__get('id'));
            $notifi->add( NOTIFI_ERROR, 'Účtenka patří jinému uživateli');
            return NULL;
        }
        
        $result = $bill->delete( $db, $id);

        if (!$result) 
        {
            $errors->add( ERROR_DELETE, 'Error update bill');
            $notifi->add(NOTIFI_NORMAL, 'Chyba smazání účtu');

            $db->disconnect();
            header("Location: " . HTTP . ACTION_BILL_EDIT . '/' . $id);
            exit;
        }
        
        $catalog = DB_Catalog::get_instance();
        $result = $catalog->delete( $db, $id);
        
        if (!$result) {
            $errors->add( ERROR_DELETE, 'Error delete catalog');
            $notifi->add(NOTIFI_NORMAL, 'Chyba smazání katalogu');

            $db->disconnect();
            header("Location: " . HTTP . ACTION_BILL_EDIT . '/' . $id);
            exit;
        }
        
        $db->disconnect();
        header("Location: " . HTTP . ACTION_LIST);
        exit;
    }
    
    /**
     * Get bill
     * @param User $user    class user
     * @param uint $id      id of bill
     * @return null         return result as array
     */
    public function get_bill( $user, $id)
    {
        $errors = Errors::get_instance();
        $notifi = Notification::get_instance();

        $db = Database::get_instance();
        $db->connect();
        
        $bill = DB_Bill::get_instance();
        
        $data = $bill->get_bill( $db, $id);       
        
        if (is_null($data) || ($db->get_num_rows($data) == 0))
        {
            $errors->add( ERROR_SELECT, 'Select no bill ' . $id);
            $notifi->add(NOTIFI_NORMAL, 'Účtenka neexistuje');
            
            return NULL;
        }
        
        $data = $db->fetch_array($data);        
        
        if ($data['id_user'] != $user->__get('id'))
        {
            $errors->add( ERROR_ID, 'Wrong id user ' . $data['id_user'] . '!=' . $user->__get('id'));
            $notifi->add( NOTIFI_ERROR, 'Účtenka patří jinému uživateli');
            
            return NULL;
        }

        return $data;                
    }

}

?>
