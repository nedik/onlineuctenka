<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'Errors.php';
require_once 'Notification.php';
require_once 'Logger.php';
require_once 'Settings/conf.php';
require_once 'Database/Database.php';
require_once 'Database/DB_User.php';
require_once 'Classes/Bill.php';
require_once 'Classes/File_manager.php';
require_once 'Settings/conf.php';
require_once 'mail.php';

/**
 * Class to operate with user
 */
class User 
{
    private static $instance = NULL;
    //identifikation of user
    private $id, $email;

    /**
     * Constructor
     */
    public function __construct() {
        $this->id = NULL;
        $this->email = NULL;
    }

    /**
     * Destructor
     */
    public function __destruct() {
        
    }

    /**
     * Return instance of class
     * @return instance of class File_manager
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) {
            self::$instance = new $class;
        }
        return self::$instance;
    }

    /**
     * Inicialization of class user
     * @return User instance of class user
     */
    public static function init() 
    {
        $o = self::get_instance();
        if (isset($_SESSION['id_user']) && isset($_SESSION['email']))
        {
            $o->__set( 'id', $_SESSION['id_user']);
            $o->__set( 'email', $_SESSION['email']);
        }
        return $o;
        
    }
    
    /**
     * Set variable to class
     * @param string $key   name of variable
     * @param string $value value of variable
     */
    public function __set($key, $value)
    {
        $this->$key = $value;
    }    
    
    /**
     * Test if user is logged
     * @return boolean  user is logged
     */
    public function is_logged()
    {
        if ((!is_null($this->id)) && (!is_null($this->email)))
        {
            return TRUE;
        }
        
        return FALSE;
    }

    /**
     * Login user to system
     */
    public function login() 
    {
        $errors = Errors::get_instance();
//        $errors->add(ERROR_TEST, 'test');
//        
        $logger = Logger::get_instance();
        //$logger->add( time(), LOG_ERROR_LOGIN, debug_backtrace(), "login");
        
        $notifi = Notification::get_instance();
        //$notifi->set( "login", NOTIFI_NORMAL);

        if (!$_POST['email'] || !$_POST['password'])
        {
            $errors->add( ERROR_LOGIN_DATA, 'You did not fill in a required field.');
            $notifi->add( NOTIFI_NORMAL, 'Nejsou vyplněné potřebné data.');
            
            return FALSE;
//            header( "Location: " . HTTP . ACTION_LOGIN);
            exit;
        }

        $username = $_POST['email'];
        $password = $_POST['password'];
        
        if (!filter_var($username, FILTER_VALIDATE_EMAIL))
        {  
            $errors->add( ERROR_LOGIN_DATA, 'Error wrong email');
            $notifi->add( NOTIFI_NORMAL, 'Email musi byt ve tvaru xx@xx.xx');
            
            return FALSE;
        }
        
        
        if (strlen($password) < 6)
        {
            $errors->add( ERROR_REGISTR_PASS, 'Password is too short');
            $notifi->add( NOTIFI_NORMAL, 'Heslo musí mít minimálně 6 znaků');
            
            return FALSE;
        }
        
        $password_md5 = $password . conf::$pass_salt;
        $password_md5 = md5($password_md5); 

        $db = Database::get_instance();
        $db->connect();

        $user = DB_User::get_instance();
        $data = $user->get_user($db, $username);
        
        if (!$data)
        {                        
            $errors->add( ERROR_LOGIN, 'Login failed ,email not exist');
            $notifi->add( NOTIFI_NORMAL, 'Přihlášení se nezdařilo, email neexistuje.');

            $_SESSION['login_username'] = $username;
            $_SESSION['login_password'] = $password;

            $db->disconnect();
            
//            $notifi->__destruct();
            
            return FALSE;
//            header("Location: " . HTTP . ACTION_LOGIN);
            exit;
        } 
        
        if ($password_md5 != $data['password']) 
        {
            $errors->add( ERROR_LOGIN, 'Login failed, wrong password');
            $notifi->add( NOTIFI_NORMAL, 'Přihlášení se nezdařilo, heslo nesouhlasí.');

            $_SESSION['login_username'] = $username;
            $_SESSION['login_password'] = $password;

            $db->disconnect();
            
            return FALSE;
//            header("Location: " . HTTP . ACTION_LOGIN);
            exit;
        }
        
        $this->id = $data['id'];
        $this->email = $data['email'];

        $_SESSION['id_user'] = $this->id;
        $_SESSION['email'] = $this->email;

        $db->disconnect();

//        header("Location: " . HTTP . ACTION_MAIN);
        return TRUE;
        exit;
    }

    /**
     * registration of new user
     */
    public function registration() 
    {
        $errors = Errors::get_instance();
//        $errors->add(ERROR_TEST, 'login');
        
        $notifi = Notification::get_instance();

        if (!$_POST['email'] || !$_POST['password'] || !$_POST['password_2']) 
        {
            $errors->add( ERROR_REGISTR_DATA, 'You did not fill in a required field');
            $notifi->add( NOTIFI_NORMAL, 'Nejsou vyplněné potřebné data.');

            return FALSE;
//            header( "Location: " . HTTP . ACTION_REG);
            exit;
        }

        $username = $_POST['email'];
        $password = $_POST['password'];
        $password_2 = $_POST['password_2'];
        
        if (!filter_var($username, FILTER_VALIDATE_EMAIL))
        {  
            $errors->add( ERROR_LOGIN_DATA, 'Error wrong email');
            $notifi->add( NOTIFI_NORMAL, 'Email musi byt ve tvaru xx@xx.xx');
            
            return FALSE;
        }
        
        
        if (strlen($password) < 6)
        {
            $errors->add( ERROR_REGISTR_PASS, 'Password is too short');
            $notifi->add( NOTIFI_NORMAL, 'Heslo musí mít minimálně 6 znaků');
            
            return FALSE;
        }

        if ($password != $password_2) 
        {
            $errors->add( ERROR_REGISTR_PASS, 'Retype your password');
            $notifi->add( NOTIFI_NORMAL, 'Hesla nesouhlasí.');
            
            return FALSE;
//            header( "Location: " . HTTP . ACTION_REG);
            exit;
        }

        $db = Database::get_instance();
        $db->connect();
        
        $user = DB_User::get_instance();
        $login = $user->insert($db, $username, $password);

        if (!$login) 
        {
            $errors->add( ERROR_REGISTR, 'Error to registr new user');
            $notifi->add( NOTIFI_NORMAL, 'Chyba registrace nového uživatele.');
            
            $_SESSION['login_username'] = $username;

            $db->disconnect();
            
            return FALSE;
//            header("Location: " . HTTP . ACTION_REG);
            exit;
        } 
        
        $data = $user->get_user($db, $username);
        $this->id = $data['id'];
        $this->email = $data['email'];

        $_SESSION['id_user'] = $this->id;
        $_SESSION['email'] = $this->email;

        $file = File_manager::get_instance();
        $file->create_folder( $this->id);

        $db->disconnect();
        
        mail_utf8($username, conf::$mail_from_user, conf::$mail_from_email, conf::$mail_subject, conf::get_mail($username, $password));        
        
        return TRUE;
//        header("Location: " . HTTP . ACTION_MAIN);        
        exit;

    }

    /**
     * Logout user from system
     */
    public function logout() 
    {
        $errors = Errors::get_instance();
        $errors->add(ERROR_TEST, "logout");

//        session_unset();
//        session_destroy();
        
        $_SESSION = array();

        $_SESSION['login_username'] = $this->email;
        $_SESSION['login_password'] = '';
        
//        unset($_SESSION['id']);
//        unset($_SESSION['email']);
        
        $this->id = NULL;
        $this->email = NULL;
        
        header("Location: " . HTTP . ACTION_FRONT);
        exit;
    }
    
    public function change_pass() 
    {
        $errors = Errors::get_instance();
        $notifi = Notification::get_instance();
        
        if (!$_POST['password_old'] || !$_POST['password'] || !$_POST['password_2']) 
        {
            $errors->add( ERROR_REGISTR_DATA, 'You did not fill in a required field');
            $notifi->add( NOTIFI_NORMAL, 'Nejsou vyplněné potřebné data.');

            header( "Location: " . HTTP . ACTION_SETTINGS);
            exit;
        }
        
        $password_old = $_POST['password_old'];
        $password = $_POST['password'];
        $password_2 = $_POST['password_2'];
        
        if (strlen($password_old) < 6)
        {
            $errors->add( ERROR_REGISTR_PASS, 'Old password is too short');
            $notifi->add( NOTIFI_NORMAL, 'Staré heslo musí mít minimálně 6 znaků');
            
            header( "Location: " . HTTP . ACTION_SETTINGS);
            exit;
        }
        
        if (strlen($password) < 6)
        {
            $errors->add( ERROR_REGISTR_PASS, 'New password is too short');
            $notifi->add( NOTIFI_NORMAL, 'Nové heslo musí mít minimálně 6 znaků');
            
            header( "Location: " . HTTP . ACTION_SETTINGS);
            exit;
        }

        if ($password != $password_2) 
        {
            $errors->add( ERROR_REGISTR_PASS, 'Retype your new password');
            $notifi->add( NOTIFI_NORMAL, 'Nová hesla nesouhlasí.');
            
            header( "Location: " . HTTP . ACTION_SETTINGS);
            exit;
        }

        $password_md5 = $password_old . conf::$pass_salt;
        $password_md5 = md5($password_md5); 

        $db = Database::get_instance();
        $db->connect();

        $user = DB_User::get_instance();
        $data = $user->get_user($db, $this->email);
        
        if (!$data)
        {                        
            $errors->add( ERROR_LOGIN, 'Change failed, email not exist');
            $notifi->add( NOTIFI_NORMAL, 'Chyba změny hesla, email neexistuje.');

            $_SESSION['login_username'] = $this->email;
            $_SESSION['login_password'] = '';

            $db->disconnect();
            header("Location: " . HTTP . ACTION_LOGIN);
            exit;
        } 
        
        if ($password_md5 != $data['password']) 
        {
            $errors->add( ERROR_LOGIN, 'Change failed, wrong old password');
            $notifi->add( NOTIFI_NORMAL, 'Chyba změny hesla, staré heslo nesouhlasí.');

            $db->disconnect();
            header("Location: " . HTTP . ACTION_SETTINGS);
            exit;
        }

        $result = $user->update_pass( $db, $this->id, $password);
        
        if (!$result)
        {
            $errors->add( ERROR_LOGIN, 'Error save new password');
            $notifi->add( NOTIFI_NORMAL, 'Chyba uložení nového hesla');

            $db->disconnect();
            header("Location: " . HTTP . ACTION_SETTINGS);
            exit;
        }
        
        
        $db->disconnect();
        header("Location: " . HTTP . ACTION_LIST);        
        exit;

    }
    
    /**
     * Get value of variable
     * @param string $name  variable name
     * @return type value of variable
     */
    public function __get($name) 
    {
        return $this->$name;
    }
}

?>
