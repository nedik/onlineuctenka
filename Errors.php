<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('Settings/definitions.php');

/**
 * Class to evidence error messages
 */
class Errors
{
    private static $instance = NULL;
    
    private $errors = array();
    
    /**
     * Constructor
     */
    public function __construct()
    {
        if (isset($_SESSION['errors']))
        {
          $this->errors = $_SESSION['errors'];
        }
    }
    
    /**
     * Destructor
     */
    public function __destruct()
    {
      $_SESSION['errors'] = $this->errors;
    }
    
    /**
     * Return instance of class
     * @return instance of class Errors
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) 
        {
            self::$instance = new $class;
        }
        
        return self::$instance;
    }
    
    /**
     * Add error message
     * @param int       $id     id of error
     * @param string    $error  error message
     */
    public function add( $id, $error)
    {
      $id = (int) $id;
      $error = (string) $error;
      $this->errors[] = array( 'id' => $id, 'error' => $error);
      

//        array_push( $this->errors, $error, null);
    }
    
    /**
     * Print all errors save in session
     */
    public function print_errors()
    {
        if (count($this->errors) > 0)
        {
            foreach ($this->errors as $key => $value)   
            {
                echo $key . ' - ' . $value['id'] . ' - ' . $value['error'] . '<br>';
            }
        }
    }
    
}

?>
