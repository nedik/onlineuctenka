<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('Settings/definitions.php');

/**
 * Class to evidence error messages
 */
class Logger
{
    private static $instance = NULL;
    private $file;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->file = conf::$log;
    }
    
    /**
     * Destructor
     */
    public function __destruct()
    {      
    }
    
    /**
     * Return instance of class
     * @return instance of class Logger
     */
    public static function get_instance() 
    {
        $class = __CLASS__;
        if (self::$instance == NULL) 
        {
            self::$instance = new $class;
        }
        
        return self::$instance;
    }
    
    /**
     * Add error message
     * @param int       $time       time
     * @param int       $id         id of message
     * @param string    $backtrace  backtrace string
     * @param string    $message    message
     */
    public function add( $time, $id, $backtrace, $message)
    {
        $id = (int) $id;
        $message = (string) $message;

        $array = end($backtrace);
        
        $data = '';
        $data .= date('Y-m-d H:i:s', $time);
        $data .= ' ' . $id . ':';
        $data .= ' ' . $array['file'];
        $data .= ' ' . $array['line'] . ':';
        $data .= ' ' . $message;
        $data .= PHP_EOL;
        
        file_put_contents($this->file, $data, FILE_APPEND);
    }    
    
}

?>